# NODE JS

#### EVENT EMITTER

```js
var EventEmitter = require("events").EventEmitter;
var loger = new EventEmitter();

loger.on("error", function(msg){ console.log("err: " + msg); )}); // will be triggered on error event-submited a event

logger.emit("error", "Message of event"); //event triggered/emitted

http.createServer(function(req,res){...});
//same as
var server = http.createServer();
server.on("request", functio...)
```

#### STREAMS

```js
request-> readable stream-inherits from evenEmitter , response -> writeable stream

http.createServer(function(req,res){
	respons.writeHead(200);
	request.on("readable", function(){
		var chunk = null;
		while(null !== (chunk = request.read())){
			response.write(chunk.toString();) //because stream obj are Buffer obj -> might be binary so need to convert it to string
		}
	}).listen(8080);

	request.on("end",function(){
		response.end();
	})
}); //node gives pipe method for reading and writing with end
//same as
http.createServer(function(req,res){
	respons.writeHead(200);
	request.pipe(response) //gets chunk writes to response;
}).listen(8080);

// creating a to read and write to
var fs = require("fs");
var file = fs.createReadStream("readme.md");
var newFile = fs.createWriteStrem("readme_copy.md");
file.pile(newFile);

//progres bar

http.createServer(function(req, res){
	var newFile = fs.createWriteStream("readme_copy.md");
	var fileBytes = request.headers['content-length'];
	var uploadedBytes = 0;

	request.on("readable", function(){
		var chunk = null;
		while(null !== (chunk = request.read())){
			uploadedBytes += chunk.length;
			var progress = (uploadedBytes / fileBytes) * 100;
			response.write("proggress : " + parseInt(progress, 10) + "%\n");
		}
	});

	request.pipe(newFile);

}).listen(8080);

//keeping stream of writeable open
By default end() is called on the destination when the source stream emits end, so that destination is no longer writable. Pass { end: false } as options to keep the destination stream open.

This keeps writer open so that "Goodbye" can be written at the end.

reader.pipe(writer, { end: false });
reader.on('end', function() {
  writer.end('Goodbye\n');
});
```

```js
//makerequest.js
var http = require("http");

var makeRequest = function(message){
	var options = { host: "localhost", port: 8080, path: "/", method: "POST"}
	var request = http.request(options, function(data){
		response.on("data",function(data){
			console.log(data);
		});
	});
	request.write(message);
	request.end();
}

module.exports = makeRequest;

/app.js
var makeRequest = require("./makerequest");

makeRequest("asd dsa");

//Express.js -> npm install --save express
var express = requrire("express");
var app = express();

app.get("/",function(req, res){
	response.sendFile(__dirname + "index.html");
});

app.listen(3000);


//least 10 tweets from twitter-needs api key

var request = require("request");
var url = require("url");

app.get("tweets/:username", function(req,res){

	var username = req.params.username;

	options = {
		protocol: "http",
		host: "api.twitter.com",
		pathname: "1/statuses/user_timeline.json",
		query: { screen_name: username, count: 10 }
	}

	var twitterUrl = url.format(options);
	request(twitterUrl, funtion(err, res, body){
		var tweets = JSON.parse(body);
		response.locals = { tweets: tweets, name: username}
		response.render("tweets.ejs");
	});
});

//req query
route_path: '/library'
actual_request_URL: '/library?userId=546&bookId=6754'
req.query: {userId: '546', bookId: '6754'} // to acces userId --> req.query.userId (req.query is an js object)
```

###Middleware
app.use(express.static(\_\_dirname + "/public"));

####chaining middleware
app.get("/asd", function(req, res, next){}, function(req, res){}); // middleware is in the middle

###Chaining more than one method for a route
app.route(path).get(handler).post(handler);

###Post Request & Body Parser
// form/post reqs are not seen in url. data is in header, default format used by HTML forms

```js
POST /path/subpath HTTP/1.0
From: john@example.com
User-Agent: someBrowser/1.0
Content-Type: application/x-www-form-urlencoded
Content-Length: 20
name=John+Doe&age=25
```

//to get header data, node uses body-parser package
//content-type: multipart/form-data for binary files(pic, music etc via form);

### new NODE.JS

```js
http
  .createServer(function(req, res) {
    response.writeHead(200); //status code
    response.write("asda asd"); //response body
    response.end(); //close connection
  })
  .listen(8080); //listen for connections on this port

const body = [];
req.on("data", chunk => {
  // data is streamed an stream has on data event to register a cb
  body.push(chunk);
});
req.on("end", () => {
  // to use chunk we need bus stops/Buffer
  // Creating a Buffer, concating it with chunks and then turning it to string(text data)
  const parseBody = Buffer.concat(body).toString();
  console.log(parseBody);
});
```

Normally node is single thread(js) and uses event loop for callbacks
but heavy lifting done with Worker Pool(different threads) which triggers callbacks

```
module.exports = funcNameEtc //or module.exports.name = ()=>
module.exports =  { key: funcName, another: "asd" } // now require as something and use something.key
module.exports.key = funcName; //or exports.key = funcNameEtc // module. can be omited with this
```

```js
npm i --save-dev/or -g nodemon //package.json scripts: start: nodemon server.js

node server.js argvStuffTo pass "for space";
//process.argv to access. 0,1 reserved. process.argv[2] and up for args
//yargs for better argv usage-objects etc -> instead of p.argv yarg.arv
// with yargs node server.js --keyname=value -> yargs.argv.keyname

//app.use to add a middleware to express
app.use(()=>{
	//res.send or res.json for sending json. .json lets you specify space and func
	res.send("<h1>Asd</h1>"); // for string header set to html auto by express
})
app.listen(3000); //express creates server and listens

//npm i --save body-parser
const bodyParser = require("body-parser");
//new express includes body parser, no need to npm i
//app.use(express.json()) : to handle raw json data
//app.use(express.urlencoded({extended: false}))

app.use(express.json())
app.use(bodyParser.urlencoded({extended: false}));

app.use("/product", (req, res, next) => { //middleware can be used as route
  console.log(res.body); //to parse body(post req data) we need body-parser and use it before routes // res.body.name
  res.redirect('/');
});

app.get("/api/members/:id", (req, res)=>{
  res.send(req.params.id);
});

app.use("/product", (req, res, next) => {
  res.status(404).send("<h1>Page not found</h1>");
});

//we can move route code to route/filename
const express = require("express");
const router = express.Router();
router.get...
router.post...
module.exports = router;

//in app/index.js
const asdRoutes = require("./routes/...");
app.use(asdroutes); // u can ad a path here, app.use('/admin', adminroutes);

//using an html file in node
const path = require("path");

router.get("/", (req, res, next) => {
	//sending a file with sendFile
	// __dirname: node's helper, absolute path of current file
	// for sendFile express auto assigns header to text/html
	res.sendFile(path.join(__dirname, "..", 'views', 'shop.html'));
});

//express crud
router.get(...)
router.put("/:id") // update request is put, req.params.id, req.body.key
router.delete("/:id") // delete request // res.redirect("/") etc
router.post(...) // sending form data

//setting a public forder for css etc
//static file requests will be served from public so path in views are already in public
//view/something.html -> css link -> "(public)/css/main.css)
app.use(express.static(path.join(__dirname, "public")));

//adding template engine
app.set('view engine', 'ejs'); //if not supported by express app.engine("nameofEngine", importedVarName) before app.set
app.set('views', 'pathToViewsDefaultIsViews');
app.render("filename"); //to use view engine files
app.render("filename", {variables: passedDown}) //variables avail in filename: usage {%= variableName %}

app.locals.nameOfGlabalEjsFuncOrVar = value; // to set a global var for views

<%- include("/layouts/head") %> // to use layouts in other ejs files
```

// Node Debugging
node inspect filename.js //or nodemon inspect
node --inspect-brk // node chrome debugging(chrome://inspect). no console debugging
n: next, c: continue(till debugger line or full script)
debugger; // for creating break points
repl// to acces break point data

// Call Stack -> main(wrapper of module) -> line by line
// -> node api -> async/delayed code is registered
// -> callback queue -> event loop -><-
call stack exed, if there is async code it moves to node api
when async code runs callback registered to callback queue
after call stack is empty event loop checks that and runs cb queue

##### SSH KEYS (google ssh-github ssh page)

ls -al ~/.ssh : to see if there is shh key//id_rsa.pub will be used
ssh-keygen -t rsa -b 4096 -C 'email@blabla.com' : generating a ssh key
eval "\$(ssh-agent -s)" : starts ssh agent / agent pid 1xxx
ssh-add ~/.ssh/id_rsa : to add ssh key to your comp
login github->settings->ssh and gpg keys
clip < ~/.ssh/id_rsa.pub : copying ssh key in terminal/windows
ssh -T git@github.com : tests connection with host

##### USING HEROKU -- login error? need new account to use

download heroku cli
npm install -g heroku
heroku login
heroku keys:add //adds ssh key to heroku
heroku keys // shows added keys.(heroku keys:remove email) to remove
ssh -v git@heroku.com // testing ssh connection

process.env.PORT || 3000 //env : to see env variables(command line)

heroku create //adds heroku remote

### AUTHENTICATION

```js
let crypto = require("crypto");

const something = "i am number 3";

const secret = "aaa333aaa333";

const hashed = crypto
  .createHmac("sha256", secret)
  .update(something)
  .digest("hex");

//npm i jsonwebtoken: used to hash objects
// jwt an object, set header
// compare req headers to see if user has acces(jwt) token
// req.header("x-auth"): gets x-auth token value
const jwt = require("jsonwebtoken");

var data = { id: 10 };
var secretWord = "something";

const testjwt = jwt.sign(dataObj, secretWord);
const decode = jwt.verify(tokenAkatestjwt, secretWord);

//custom headers starts with x
//sending jwt to client as header // can get id from .verify
// status 401: auth required
res.header("x-auth", testjwt).send(userEtc); //post user/login route

var authenticate = (req, res, next) => {
  //...
  req.asd = "something";
};

app.get("/path/to/somewhere", authenticate, (req, res) => {
  //can acces req.asd here: added to request object
  //after patch a middleware func can be specified
});
```

#### HASHING/SALTING PASSWORD

```js
onst bcrypt = require('bcrypt');

const saltRounds = 10;
const password = 'yourpass';

// technique 1
bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(myPlaintextPassword, salt, function(err, hash) {
        // Store hash in your password DB.
    });
});

// tecnique 2
bcrypt.hash(myPlaintextPassword, saltRounds, function(err, hash) {
  // Store hash in your password DB.
});

// with promise

bcrypt.hash(myPlaintextPassword, saltRounds).then(function(hash) {
    // Store hash in your password DB.
});

// with promise
bcrypt.compare(myPlaintextPassword, hash).then(function(res) {
    // res == true
});

// To check a password:
bcrypt.compare(myPlaintextPassword, hash, function(err, res) {
    // res == true or false
});


//example bcrypt
const correctPassword = (enteredPassword, originalPassword) => {
	return bcrypt.compare(enteredPassword, originalPassword)
.then(res) =>{
	return res;
});

User.findOne({
  where: {
    username: userDetails.username
  }
})
.then((user) => {
  return correctPassword(userDetails.password, user.password);
})
.then((authenticated) => {
  res.send(authenticated);
})
.catch(e) {
  res.status(400).send(e)
}
```

#### SESSION

```js
const session = require("express-session");
//instead of keeping session in memory we can use db
const MongoDbStore = require("connect-mongodb-session")(session);

const store = new MongoDbStore(
	uri: "connectionString",
	collection: "sessionTableToStore",
	expire: "etcOptions"
);

app.use(session({secret: "your secret", resave: false, saveUninitialized: false,
//with store option data will be stored in db
store: store
}));

app.get("/etc", (req, res)=>{
	req.session.something = "something, num, boleean etc";
//req.session.destroy(()=>//will be called when session destroyed, redirect etc); for destroying session
})
```

### SOCKET IO

```js
//npm install --save socket.io
const socketIo = require("socket.io");
var app = express();
const http = require("http");

const app = express();
app.use(express.static(path.join(__dirname, "public")));
const server = http.createServer(app);
const io = socketIO(server); //url/socket.io/socket.io.js added to client

// registering an event // connection requests keeps coming from client if server is down as long as client page is open
io.on("connection", (socket) => {

	//io.emit emmits to all conenction
	io.emit("nameOfCustomEvent", {data: "data stuff"); //second arg: optional data

	//custom events
	socket.on("createEmailcustomEvent", (newEmail)=>{
		console.log(newEmail);
	})
	socket.emit("emitingCustomEvent", {data: "data"});
}); // there is also built in disconnect event

//broadcasting: emits an event all but that socket
socket.broadcast.emit("customEventName", data);

//index.html
<script src="/socket.io/socket.io.js"></script>
<script>
	var socket = io(); //creates a connection to server

	socket.on("connect", function(data){
      console.log("connected to server")
		})

	socket.on("nameOfCustomEvent", function(dataHere){ });

	// firing/emiting events from socket

	socket.emit("createEmailcustomEvent", {data: "object"});
	socket.on("customEventName", function(data){});
</script>
```

### Web Scrapping

```js
//puppeteer: headless browser(google)

const fs = require("fs");

const request = require("request-promise");
const axios = require("axios");
const cheerio = require("cheerio"); //jquery like lib to acces html

//axios call with headers
let $ = cheerio.load(response.data); //html response is processed by cheerio

let title = $("div[class='title_wrapper'] > h1")
  .text()
  .trim();
//...

// to get a file/img we need a stream
let file = fs.createWriteStream(`${movie.id}.jpg`);

axios({
  method: "get",
  url: posterImg,
  responseType: "stream",
  headers: {
    /*headers*/
  }
}).then(function(response) {
  response.data.pipe(file);
});

//react site etc-> genereted classnames etc
// network xhr request
// view page source for initial page before react etc

// npm i puppeteer --save : google puppeteer
const puppeteer = require("puppeteer"); // for normal and headless browser
const axios = require("axios");
const cheerio = require("cheerio"); //to get info from full html content //.eq(3) to get third elem from arr?
//let $ = cheerio.load(response.data); // to parse html string then // $("selector") to get elems

// to get a file/img we need a stream
let file = fs.createWriteStream(`${movie.id}.jpg`);

axios({
  method: "get",
  url: posterImg,
  responseType: "stream",
  headers: {
    /*...*/
  }
})
  .then(function(response) {
    response.data.pipe(file);
  })
  .catch(e => console.log("something went wrong", e));

// csv
// const Json2csvParser = require("json2csv").Parser;
// const parser = new Json2csvParser();
// const csv = parser.parse(movieData);
// fs.appendFileSync("./data.csv", csv, "utf8");

//puppeteer
const puppeteer = require("puppeteer");
//to use mobile device view
const devices = require("puppeteer/DeviceDescriptors");
//to ignore https errors
const browser = await puppeteer.launch({
  ignoreHTTPSErros: true, //to ignore https/ssl errors
  devtools: true, // to open dev tools in browser
  defaultViewport: { width: 1920, height: 1080 }, //changing default viewport size
  args: ["--proxy-server=167.99.16.93:3128"] //changing ip with public proxy server ip
});

const page = await browser.newPage();
//emulate a mobile device
await page.emulate(devices["iPhone 6"]);
await page.goto("https://google.com");

//accesing dev console of chromium and types/evaluates callback code
let user = await page.evaluate(() => {
  return document.querySelector("h1.ProfileHeaderCard-name > a").innerHTML;
});

//page.$$ -> The method runs document.querySelectorAll within the page.
//page.$('css selector') -> runs document.querySelector within the page
let tweetsArray = page.$$("#stream-items-id > .stream-item");

//.$$ returns ElementHandlers? returned tweetsArray -> forof loop -> tweet
// to further actions we need tweet.$eval?
// like second querySelectorAll? on first one?
let tweetText = await tweet.$eval(
  ".js-tweet-text-container",
  elem => elem.innerText
);

// scrooling down to bottom //check last item length to prevent infinite scrooling
while(tweetsArray.length < countWeWant){
	await page.evaluate(() => {
		window.scrollTo(0, document.body.scrollHeight);
	});
	await page.waitFor(3000);
	//check length again
	tweetsArray = await page.$$("#stream-items-id > .stream-item");
}
// or a string, runs a js script in browser
await page.evaluate("window.scrollTo(0, document.body.scrollHeight)");

//to print evaluate console.log(in browser) to our console
page.on("console", msg => console.log(`msg from browser console ${msg.text()}`));

//waits till selector in page
await page.waitFor("a[href='/accounts/login/?source=auth_switcher']");
await page.click("selector");

//aborting image request, faster loading time
await page.setRequestInterception(true);
if (["image", "font", "stylesheet"].includes(request.resourceType())) {
  request.abort();
} else {
  request.continue();
}

//passing basic auth
await page.authenticate({ username: "admin", password: "123456" });
await page.goto("https://httpbin.org/basic-auth/admin/123456");

//await page.waitForNavigation();
//await page.waitFor(2000);

//getting elem and then clicking instead of page.click
// const inputElem = await page.$("button[type='submit'");
// await inputElem.click();

//must be headless, parsing as pdf
// await page.pdf({ path: "./page.pdf", format: "A4" });

//getting title and url, works if it is not headless, headless is false
// let title = await page.title();
// let url = await page.url();
//console.log(title, url);

// await page.type("selector", "hmm", { delay: 100 });
// await page.keyboard.press("Enter");

// await page.waitForNavigation();

// await page.screenshot({ path: "example.png" });
//await browser.close();
```

### CORS server setting (to let other non internal pages to acces api)

```js
app.use((req, res, next) => {
	//who can acces: origin, which methods allowed: methods, which headers client can send back: headers
  res.setHeader("Acces-Control-Allow-Origin", "*");
  res.setHeader("Acces-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE");
  res.setHeader("Acces-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});
```