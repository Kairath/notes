```php
$hereDoc = <<<A_LABLE
strings
vars etc
A_LABLE
```

```php
$a = &$b //reference assigment
```

```php
function aFunc($myArg1, $myArg2, $argWithDefaultValue = "asd"){ #code }
```

```php
declare(strict_types = 1); //duck typing off -> "7" cant be converted to int
```

```php
function aFunc() :int { #code } // php 7: func return types can be declared
```


```php
function aFunc(...$argArray) { $argArray[0]; $argArray[1];} // arg separation
```

- a func inside a func (local func) => outside func must be called first
- a $var inside func cant be accesed = local
- for acccesing -> global varname; or $GLOBALS["varname"] = "value"

- a static $var declared locally does not lose its value when method exits


##### Anonym Funcs / Closures
```php
$myFunc = function(){}
//for using parent variabla=>use keyword
$myFunc = function() use($aVar) {}
```

---

```php
call_user_function("funcName", "othersArgs", "secondArg");
$$var //variabla var or ${$foo}
```

---

```php
function myFunc(){}
$var = "myFunc";
if(is_callable($var) { $var(); } //variable function
```

##### define($var) // checks if a var exist

```php
const ASD = "value"; //since php 5.0
constant("ASD"); // returns value
```


##### in loop, break: breaks the loop - continue: goes to next iteration

##### (condition) ? "asd" : "aaa"; // ternay operator

```php
//since 5.4, we can use [...] for array
// count($arr); 
// is_array($arr); 
array_key_exists('key', $arr) // if key exists returns true
// array_keys($arr); // returns keys of an arr
```

```php
printf("a string %s funny parts will be %s". "this if for first", "placed") 
// first string's %s will be replaced with other args
```

##### direname(__DIR__); // relative or absolute path of current dir
##### isset($foo);   is_null($bar);



```php
try{} catch(Exception $e) {} finally {}

// throwing exception with custom message
throw new Exception("an exception message");

//custom Exception
class myException extends Exception{
 function __construct($msg){
    parent::__construct($msg);
 }
}
throw myException("msg");
```

##### namespaces

-file1.php
```php
namespace foo;
class Cat {}
```
-file2.php
```php
namespace bar;
const ABC = "abc";
```
-file3.php
```php
include "file1.php";
include "file2.php";

echo bar\ABC;
$a = new foo\Cat();
```
-file3.php
```php
include "file1.php";
include "file2.php";

use foo, bar as zzz

echo bar\ABC;
$a = new foo\Cat();
```


- multiple namespace
```php
namespace myN1{}
namespace myN2{}
namespace{ #global, unnamed}
```

- $_FILE // files saved to a temp location
```
move_uploaded_file() // for saving it
file_get_contents // getting contents of file
```

- Generator syntax // returns object instead of single value
```
function gen_one_to_three() {
    for ($i = 1; $i <= 3; $i++) {
        // Note that $i is preserved between yields.
        yield $i;
    }
}

$generator = gen_one_to_three();
foreach ($generator as $value) {
    echo "$value\n";
} // echoes 1 2 3 


function gen_three_nulls() {
    foreach (range(1, 3) as $i) {
        yield;
    }
} // yields nothing=null -> { null, null, null }

var_dump(iterator_to_array(gen_three_nulls()));
```

```
// maps over arrs elements and does operations like changing an keys value
// can be used to cast obj to array, or can mutate data
$mapsOverAnArrayAndReturnsWhatchaWhant = array_map(function($eachMember){
  return "bla bla bla";
}, $ofThisArray); // returns arr lenght * "bla bla bla"

// array_column($arr, "key"); returns elements that value only

$arrFilter = array_filter($arr, function($arrMember){
    return $arrMember["completed"] == false; // 
); // will return true ones, filters array
// return ! $arrMember["completed"] == false // for other ones
```

```
// if there is a path like
// $url = "localhost/contact?asd=zzz"
trim(parse_url($url, PHP_URL_PATH), "/"); // returns /contact then trimmed to contact

$_SERVER['REQUEST_METHOD']; // gives request method // post, get...
```

##### sessions
```
file1.php
----
session_start();
$_SESSION["foo"] = "bar";

file2.php
---
session_start();
print_r($_SESSION); // there is out foo;
```
