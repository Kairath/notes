#CSS

- do agains: transition, animation, transform, @media
- min-resolution: 300dpi-retina image

###css variable
--varname: value;

background: var(--varname);
background: var(--varname, black); // if var is not working, fallback value is black

####global css variable
:root {
// define css vars here to acces everywhere, html element
--penguin-belly: pink;
} // value can be set differently in element selectors

### Selectors

em.date: ems with date class will be selected
ul .asd: asd class elements after ul will be selected
a:hover: pseudo selectors after :
a:visited, input:focus, li:last-child (last li element), li:nth-child(2), li:nth-child(3n+1)//1, 4, 7 , li:nth-child(odd|even)

###Css Attr Selector

```
a[attrname] {} // selects a tags with attrname attribute ex: a[target] wil select a elements with targ attr
a[attrname="blabla"] // a[attribute~="flower"]: selects a' which has flower in their val(not abc-value or values)
[class|="top"] // has to be whole word, either top or top-: topblabla will not be selected
[attribute^="value"]: attrs starts with val, not has to be whole word value-, value, valueblabla all okay
[attribute$="value"]: attrs ends with value
[attribute*="value"]: attr has value in their value: [class*="te"] will select team etc but not second
```

### Position

```css
position: static|relative|absolute|fixed
- static: normal flow of document
- relative: relative to its original position
- absolute: point of reference is the first non static(positioned) ancestor.
- fixed: point of reference is the viewport/browser

// if an element is positioned then it can use left-right-bottom-top
// if both left and right set it will stretch unless width is set
// if both left and right set and has a width, right will be discarded

- outline: none; removes outline from an input
```

- if an element has fixed width and height it might overflow
- overflow: hidden // to hide overflow
- overflow: auto // overflow part can be scrooled

- display: none // will removed from doc flow
- visibility: hidden; // will not be shown but takes space still

### Float: none|left|right

- floated element is out of the flow-> if width is longer it will overflow
- floated elements will have a display block applied to them auto
- if no height is set element's height is line height
- clear can be applied to block -> clear: both|left|right

```css
/*parent element should have group claass. ie 8 and up */
.group:after {
  content: "";
  display: table;
  clear: both;
}

/*mdn */
#container::after {
  content: "";
  display: block;
  clear: both;
}

/*shay howe's site*/
.group:before,
.group:after {
  content: "";
  display: table;
}
.group:after {
  clear: both;
}
.group {
  clear: both;
  *zoom: 1;
}

---

/* FULL - csstricks */
.cf:before,
.cf:after {
  content: " "; /* 1 */
  display: table; /* 2 */
}

.cf:after {
  clear: both;
}

/* For IE 6/7 only */
.cf {
  zoom: 1;
}

/* new clearfix but not widely supported */
.container {
  display: flow-root; //new shit
}
```

### RangeSlider

```css
.slider{
  -webkit-appearance: none;
  appearance: none;
  width: 25px; /* etc */
}
.slider::-webkit-slider-thumb { /* -webkit- (Chrome, Opera, Safari, Edge) and -moz- (Firefox)  */
  -webkit-appearance: none;
  appearance: none;
  width: 23px; height: 24px;
  border: 0; outline: none;
  background: url('contrasticon.png');
```

### Parallax

```css
.parallax {
  /* The image used */
  background-image: url("img_parallax.jpg");

  /* Full height */
  height: 100%;

  /* Create the parallax scrolling effect */
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
```

### ScrollTop

```js
/* When the user scrolls down 20px from the top of the document, show the button */
window.onscroll = function() {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("myBtn").style.display = "block";
  } else {
    document.getElementById("myBtn").style.display = "none";
  }
}

/* When the user clicks on the button, scroll to the top of the document */
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
```

#### Progress Bar: Shows where scroll is

```js
// When the user scrolls the page, execute myFunction
window.onscroll = function() {
  myFunction();
};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height =
    document.documentElement.scrollHeight -
    document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}
```

#### CSS Image filter effects

`imgSelector {filter: grayscale(100%);}` // filter: grayscale(%)/blur(px)

### Css Sticky Navigation

## favicon // 16x16, 32x32, 48x48 etc ico file, just putting favicon.ico to root of your domain works without html tag?

`<link rel="shortcut icon" type="image/png" href="/favicon.png"/>` // with a favicon generator etc png can be converted to .ico

retina image: size of image x-x, set a width/height of x/2 or img { transform: scale(0.5)}

```js
function stickyNavbar() {
  var stickyNav = document.querySelector(".navbar-sticky");
  var offSet = stickyNav.offsetTop;

  if (window.pageYOffset > offSet) {
    stickyNav.classList.add("sticky");
  } else {
    stickyNav.classList.remove("sticky");
  }
}

// if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) { navbar.style.top = "0"; } ... : navbar/top button on scroll
```

### Fullscreen Video

`<video autoplay muted loop id="video"> <source src="video/asdf.mp4" type="video/mp4"> </video>`

```js
function toggleVideo() {
  var video = document.getElementById("video");
  var button = document.querySelector(".video-button");

  if (video.paused) {
    video.play();
    button.textContent = "Pause";
  } else {
    video.pause();
    button.textContent = "Play";
  }
}
```

### Copy Text

```js
function copyText() {
  var textInput = document.getElementById("copy-input");
  textInput.select();
  document.execCommand("copy");
}
```

### Flip box <div class="flip"><div class="flip-box"><div class="front">Front</div><div class="back">Back</div></div></div>

```css
/* rotateX for vertical flip */
.flip {
  width: 300px;
  height: 250px;
  background-color: transparent;

  &:hover .flip-box {
    transform: rotateY(180deg);
  }

  .flip-box {
    width: 100%;
    height: 100%;
    perspective: 1000;
    background: #ddd;
    position: relative;
    text-align: center;
    transition: transform 0.8s;
    transform-style: preserve-3d;
  }

  .front,
  .back {
    position: absolute;
    width: 100%;
    height: 100%;
    backface-visibility: hidden;
  }

  .front {
    background-color: #bbb;
    color: black;
  }

  .back {
    background-color: dodgerblue;
    color: white;
    transform: rotateY(180deg);
  }
}
```

### Triggering a button on input-enter

```js
var input = document.getElementById("myInput");

input.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    event.preventDefault();
    document.getElementById("myBtn").click();
  }
});

navigator.onLine; // true if browser is in online mode, false for offline
```

### Link href preventing

`<a href="javascript:void(0)">or javascript:;</a>` // to support very old browsers, or use return false/e.preventDefault()

#ANIMATION

transition: <prop> <duration> <timing-func> <delay> <iteration-count> <direction> <fill-mode>
def-all|only needed|def-ease | def-0 | def-1 | def normal | default: none

transition: <property> <duration>, <prop> <duration>
transition: all 0.4 -> all props can be used

for human eyes -> 0.256s if seenable alt limit

---Prefixes---
-webkit-transition: background-color .4s;
-moz-transition: background-color .4s;
-o-transition: background-color .4s;
transition: background-color .4s;

---

<section>
	<a href="" class="btn btn-primary">
		<span class="top content">Buy Now<span>
		<span class="bottom content">On Sale<span>
</a><section>

.btn { position: relative; }
.content: { position: absolute; }
.top: { top: 0; } .btn:hover .top { top: -100px; }
.bottom: { top: 100px; } .btn:hover .bottom { top: 0px; }
.content{ position: relative; transition: top 0.3s; overflow: hidden; }

<!-- \*\*\*display: none; -> not transitionable use opacity: 0; visibility: hidden(for preventing click events) -->

### Transform : Doesn’t affect the Flow

transform: translate(moves x y z), rotate, scale(resizes), skew(disorts)
transform-origin allows to modify the origin point
transform-style is for 3d settings

transition: transform 4s;
:hover -> transform: rotate(360deg); //or 1turn /unix suffix

---Scale---
to stretch an element based on the value multiplier

transform: scale(2);-> doubles size
transform: scale(4,2); -> x *4 , y *2

tranform: scaleX(value); tranform: scaleY(value); for scaling separetly

**\*scaled elems maintains orginal box-model size**
1 way of solving-> transform-origin: center left; (y,x)

---Translate---
for moving elements

transform: translateX(3px); -> moves right 3px

---Keyframe---
A list of what should happen over animation

@keyframes <custom-own-name-for-animation> {
<step1> { <property>: value; }
<step2> { <property>: value; }
}

@keyframes swing {
0% { transform: rotate(0deg);} //or from for 0
20%,50 { transform: rotate(10deg);}
30% { transform: rotate(15deg);}
100 { transform: rotate(-10deg);} //and to for 100
}

#left-arm {
transfor-origin: top center; //for changing where to rotate
animation: swing 2s(duration) 0s(delay) infinite(iteration) ease;
}

.modal { //normallu goes orginal state when anim finished
animation: fadeIn .25s forwards(fill-mode)(def none)
}

.modal-active { //overshoot effect
animation: slideUp .65s .5s cubic-bezier(0.17, 0.89, 0.32, 1.28) forwa
}

### FlexBox

##### Container Props

display: flex/inline-flex; //makes direct children flex items

flex-direction: row/column/row-reverse/column-reverse
flex-wrap: nowrap/wrap/wrap-reverse // multiline-default is nowrap
flex-flow: shorthand for flex-dir flex-wrap
justify-content: flex-start/flex-end/center/space-between/space-evenly/space-around (horizontal space)
align-items: flex-start/flex-end/center/baseline/stretch (default: stretch, vertical alignment)
align-content: flex-start/flex-end/center/stretch/space-around/space-between (vertical space, works if 1+ row)

##### Flex Item Props

order: -1; //default is 0.
flex-grow: 0; // how much an item can grow
flex-shrink: 1; // how much an item can shrink
flex-basis: auto; // default size of item
flex: f-grow f-shrink flex-basis //shorthand, last 2 optional
align-self: flex-start/end/center-baseline/stretch (overrides parents align items value)

flex none = 0 0 auto ( grow shrink basis)
flex initial = 0 1 auto ( def values)
flex none not allows to shrink = potential item overflow

######Aligning to center
//or instead of margin auto you can specify justify-content:center and align-items: center in parent

```
.parent {
  display: flex;
  height: 300px; /* Or whatever */
}

.child {
  width: 100px;  /* Or whatever */
  height: 100px; /* Or whatever */
  margin: auto;  /* Magic! */
}
```

######NavLinks Large:justify left, medium:justify around, small: links are stacked

```css
/* Large */
.navigation {
  display: flex;
  flex-flow: row wrap; //wrap will allow them to stack when it is small screen
  /* This aligns items to the end line on main-axis */
  justify-content: flex-end;
}

/* General CSS rules for every viewport, including smartphones */

@media (min-width: 768px) {
  /* Rules for tablets and bigger viewports */
}

@media (min-width: 992px) {
  /* Rules for laptops, small desktop screens and bigger viewports */
}

@media (min-width: 1200px) {
  /* Rules for larger desktop screens only */
}

/* Medium screens */
@media all and (max-width: 800px) {
  .navigation {
    /* When on medium sized screens, we center it by evenly distributing empty space around items */
    justify-content: space-around;
  }
}

/* Small screens */
@media all and (max-width: 500px) {
  .navigation {
    /* On small screens, we are no longer using row direction but column */
    flex-direction: column;
  }
}

// at least 1000px and landscape mode (as opposed to portrait mode)
@media (min-width: 1000px) and (orientation: landscape) {
}
```

######Telling all items to be 100% width via flex-basis

```
/* We tell all items to be 100% width, via flex-basis */
.container > * {
  flex: 1 100%; /*all can grow to 1 unit, flex-basis is 100 so it takes whole row
}
```

#####Sass Mixing for Flex prefixes

```
@mixin flexbox() {
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
}

@mixin flex($values) {
  -webkit-box-flex: $values;
  -moz-box-flex:  $values;
  -webkit-flex:  $values;
  -ms-flex:  $values;
  flex:  $values;
}

@mixin order($val) {
  -webkit-box-ordinal-group: $val;
  -moz-box-ordinal-group: $val;
  -ms-flex-order: $val;
  -webkit-order: $val;
  order: $val;
}

.wrapper {
  @include flexbox();
}

.item {
  @include flex(1 200px);
  @include order(2);
}

```

#SASS

`npm i --save-dev node-sass`
`"scss": "node-sass -o scss/ css/` package.json scripts
`npm run scss` to run scss script

`sass --watch app/sass:public/css` watches scss files in app/sass and conv into pub/css

@import "buttons" -> importing \_button.scss file
& -> parent a:hover -> a { &:hover}
\$base: #d3d3d3; -> varable declaration

$title: "my";
$title: "about" !default; -> sets to def so not overrides first

variables have scope.if declared in decoration cant be used outside

#{\$variable} ->string interpolation

@mixin mixinname {
asd: sda
...
}

btn-a {
@include mixinname
background: #777
}

@mixin box-sizing($x: border-box){ -> default val for var can be used without argument
	-webkit-box-sizing: $x;
...
}

.content {
@include box_sizing(border-box);
}

@mixin transition($val...) -> for muultiple comma separeted args
in declaration $var... -> for separating values as args

.btn-a {
...
}

.btn-b {
@extend .btn-a -> takes rules of btn-a
background: #ff0;
}

%btn { -> can be extended but not a selector by its own
...
}

@function fluidize($a , $b){
@return ($a / $b) \* 100;
}

.sidebar {
width: fluidize(100, 20);

@if condition {
...
} @else if condition { ... } @else { ... }

@each $a in $as { ... }
@for $i from 1 through 3 { ... }
@while $i <4 { }

lighten(\$color, 20%);
darken...
saturate()
desaturate
mix

.sidebar {
border...
@media (min-width: 700px) { -> uses this rules size
...
}

@mixin respond_to {
@media..{
@content
}
}

.sidebar {
...
@include respon_to {
...
}
}
...
}

###Bootstrap
####Html responsive tags + bootstrap js and css files

```
// add meta and css files in head
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<!-- Bootstrap Css -->
<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">

//before </body> tag add js files
<script src="node_modules/jquery/dist/jquery.slim.min.js"></script>
<script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
```

`align-items-center`: aligning items verically now possible(bootstrap 4)

####Icons-Social Icons
Font-awesome and bootstrap-social used

```
<a class="btn btn-social-icon btn-linkedin" href="http://www.linkedin.com/in/"><i class="fa fa-linkedin"></i></a>
```

- loading public fonts

```css
@font-face {
  font-family: "Lobster";
  src: local("Lobster"), url("lobster.woff") format("woff");
}
body {
  font-family: "Lobster", "Comic Sans", cursive;
}
```
