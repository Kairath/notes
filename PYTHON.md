PYTHON

```python
help(len) or dir(sys) for help #what module has
python filename # running .py files in terminal
import name_of_the_module as rename_module_name
print("Hello World\nNewLine\tTab")
print(1, "hmm", aVar, sep = "-") # 1-hmm-aVarValue
print(*"Python") # P y t h o n //sep = "-" -> P-y-t...
#single line comment: """doc string, info about whole page, or multiline comment/string"""
#nums are truthy, except 0

numbers : int, float, scientific(e notation), inf(infinite), float(3) (turns 3 to 3.0), int(3.7) #becomes 3
3/2 = 1.5(float) # 3 // 2 = 1 (integer division, decimal part omited)
3 ** 2 = 9

### String
# string -> "stri'ng", 'str\'ing', """string"""
# str[0], str[-1] #last, str[-2] #second last
# [start : exclusiveEnd : increase ] #[::3] third char: 0, 3
# str[:3] #till 3, str[3:] #after 3, str[:] #full,
# [::-1] - reversing a string #[:-1] -getting till last char
# len(str) # str + str2 # str * 3 //strstrstr
# "{} + {}'nin toplamı {}'dır".format(3, 4, 7) //3 + 7'nin...
# "{1} {0} {2}".format(a, b, c) // b a c
# "{:.2f} {:.3f}".format(3.1454, 5.3248) // 3.15 5.325

### List/Array
list = ["hmm", 3]
#len(arr) #list(str) #list[4:] #list[::2] #list[::-1]
#arr3 = arr1 + arr2 //concatination
#list * 2 // ["hmm", 3, "hmm", 3]
#list[:2] = ["change first", "values"]
#append/clear/copy/count/extend/index/insert/pop/pop(index)/remove/reverse/sort/sort(reverse=True)

#### List Comprehension
lista = [1,2,3] # listb = [i for i in lista] #copying items
list = [[1,2,3], [4,5,6]] # for tubles its for i, j in list
list2 = [x for i in list for x in i] # nested

tupleVar = (1, 2, 3) #values cant be changed, type(t) //tuple
#tuple methods: non assigment/mutation methods of array
tuppleVar.count(1) #how many 1 is in the tuple //1
tuppleVar.index(3) #what is index of 3 //2

dictVar = {"key": "value", "key2", 2} #js object
dictionary["key"] #value
dictVar.keys() / dictVar.values() / dictVar.items() #key-val
#for k, v in dictVar.items(): #print(k,v)

### Changing type -> str(num), int(str), float(num/str)

###Variables: 8asd, !&.(except _), no space/reserved word
varName = value or var_name = value
print("a" + var_name + "b") #throws error, cant conc str with nums
print("a" + str(int_var)) #correct way

a, b = 3, 4 # multi var assigment / or a, b = b, a : a gets b

# no i++, i += 1 works (*= etc)
# comment: single(#), multi(""" comment """)
# type(3.13) //float - type(str) // str

### Getting user inputs
a = int(input("your age")) #user input is always string

### Try catch -> try expect
try:
  a = int(input("a:"))
  print(a)
except ValueError:
  print("Please use a number")

bool(0) #0 and 0.0 is False, others True

a = None # null of js, it is gonna be asigned later

### Conditional
# == != <= >= < >
# and or not(not a!=b) # short circuit

if condition1: #or if (condition1)
  print("lalala")
elif condition2:
else:
  pass # means do nothing

### and or not
not True = false - a and b - a or b

### Function -default return value is none-
def funcName(input1, input2="default value"):
  #code
  return "something"

def funcName(*a): #*a: is like ...args in js
  print(a) #1, 2, 3
funcName(1, 2, 3) #a will become all passed args

#### Global/Local(only funcs and classes) vars : vars in if/loops are global

c = 3
def aFunc():
  c = 2 #new local variable
  print(c) #2
aFunc() #prints 2, after function execution local vars removed from memory
print(c) #3

def aFunc():
  global c #use global c variable, changing global vars is not a good practice
  c = 2 #new local variable
  print(c) #2
aFunc() # prints 2
print(c) # prints 2

 pi = 3.14
  ##text = 'The value of pi is ' + pi      ## NO, does not work
  text = 'The value of pi is '  + str(pi)  ## yes

  s.lower/upper/trim
  s.isalpha() / .isdigit() / .isspace()
  s.startswith("as") / .endswith("as")
  s.find("xas") -> returns first index or -1 if not found
  s.replace("old", new)/split("delimeter")/join(list)
  s[1:4] is 'ell' -- chars starting at index 1 and extending up to but not including index 4 #slice
  s[1:] s[:]->gives copy of string
  s[:n] + s[n:] == s

### LOOPS: break/continue is like in js
5 in [1,2,3,4] #False, 5 is not in arr, "p" in "python" true
not 5 in [1,2,3] #True
for (i, j) in [(1,2), (3,4)] # tuple in array -> 1,2 - 3,4

for elem in data_structure_like_array_etc

while(2 > 1):
  # code


### Range
print(*range(0, 10)) # to print we need * in front, 0, 1, 2, ..., 10 # range(0,15) == range(15)
print(*range(1, 10, 2)) # 1, 3, 5, 7, 9
range(20, 0, -1) #without -1 its not gonna work: default is increasing
for i in range(1, 3): # 1, 2, 3
  print(i)

### Lambda: to declare an inline function
label = lambda param1, param2, ... = #code

multiplyByTwo = lambda a: a*2 # : a*2 means return a*2
print(multiplyByTwo(2)) #4

### Modules
import math # dir(math) : module's methods
from math import * # help(math): methods with description
from math import floor, factorial # importing just 2 methods
#if you declare a factorial func and then import math's factorial
#Python will use last function which is math's factorial

## Creating modules
# if we put module file in c:/../Pytohn/Lib we can acces from any file
# moduleTest.py
programming_langs = ["python", "js"]

def sayHi(name):
  """
  @name: string
  @print: string => 'Hello, name'
  """
  print("Hello ", name)

# anotherFileInSameFolder.py
from moduleTest import sayHi, programming_langs
#or import moduleName -> moduleName.prog_langs
sayHi("test")
sayHi(programming_langs)

rand_num = random.randint(1, 40) #import random, inclusive
time.sleep(10) #import time: code sleeps for 10 seconds
time.time() #current timestamp/dif since 1970 in secs
```

### OOP

```python
class Something():
  aStaticProp = "poo" #Something.aStaticProp or instance.aS
  def __init__(self, name="defaultValue", age = 300): #init initiated when class used
    self.name = name
    self.age = age

  def aClassMethod(self):
    #code

instance = Something("hmm", 200) #init ran
instance.aStaticProp
Something.aStaticProp

### Inheritance
class AnotherThing(Something): #like js's extend Something
  pass # means i will write rest later, to prevent error

b = AnotherThing("inherited name", 300)
b.name #inherited constructor/name

### Overriding: when we declare inherited methods with same name its overridden
class YetAnother(Something):
  def aClassMethod(self, x): #inherited method overriden
    return x

### Using 'super' to override methods
class YetAnotherOne(Something):
  def __init__(self, name, age, extra):
    super().__init__(name, age)
    self.extra = extra
  def __len__(self): #len(instanceName) now will work
    #code, __len__ is not auto assigned like __str/init__
  def __del__(self):
    print("its being deleted") #becomes extra code for del

### Deleting an instance
del instanceName #obj is removed

# python assigns __init__ etc if it is not declared
# __str__(self) method is used when called with print
# methods like __len__ not auto assigned by Python
# __del__ / del instanceName: auto asigned but we override its not totally overriden, instead expanded
```

### Exception Handling - try, except, finally

```python
try:
 #raise ValueError()
 #or raise Exception("optional message")
 # if exception raises, rest of the code in block wont run
except ValueError:
  print("this will run for value errors, except: wont if its value error")
except (aError, bError):
  #code for multiple errors expect block
except: ## for all errors this block will run if not catched
  #code
finally:
  #some closing code
```

### Files

```python
open(file_name, mode) #mode: r: reading, c: create
#mode: w->if file exists it rewrites/creates
#mode: a->if file exists cursor goes to end of file(no new)
#mode: r+: reading and writing files
file = open("C:/Users/USER/Desktop/lululu.txt", "w", encoding="utf-8") #encoding utf-8 for using non eng chars
file.write("something")
file.close()

### Reding file
for line of file:
    print(line, end="") #end="" for preventing /n from print

content = file.read() #if we read again it will be empty, cursor is at the end second time
line = file.readline()
linesArr = file.readlines()

### Auto closing file
with open("C:/Users/USER/Desktop/filename.txt", "r", encoding="utf-8") as file:
  for i in file:
    print(i)

with open("filename.txt", "r", encoding="utf-8") as file:
  file.seek(5) #goes to byte 5
  file.tell() #tells which byte we are at, 5
  file.read(10) #reading 10 bytes
  file.tell() #15 : 5seek, 10read
  file.seek(0) #moving cursor to start

#writing at 1st line
with open("C:/Users/USER/Desktop/lululu.txt", "r+", encoding="utf-8") as file:
  content = file.read()
  content = "eee\n" + content
  file.seek(0) #without this it will be appended at end
  file.write(content)

#writing to nth line
with open("C:/Users/USER/Desktop/lululu.txt", "r+", encoding="utf-8") as file:
  contentArr = file.readlines()
  contentArr.insert(3, "new elem")
  file.seek(0)
  for i in contentArr:
    file.write(i)
```

### Python's map, filter, reduce

```python
# map(fn, list_tuple_etc_iterable) #returns a map object
list(map(lambda x: x*2, "python")) #list() turns map to list
map(lambda x, y: x*y, list1, list2) #list1[0] and list2[0] first args of lambda aka x,y

#reduce(fn, iterable_list_etc), returns reduced value
from functools import reduce #reduce method moved to functools in later versions(2,7 etc)
reduce(lambda x,y: x + y, [1, 2, 3, 4])

#filter(fn, iterable) #returns a filter obj with true vals
list(filter(lambda x: x > 5, [3, 7, 8])) #[7, 8]

#zip(list1, list2, l3) ; creates a new zip obj for each elem
list(zip(l1, l2)) #[(l1[0], l2[0]), (l1[1], l2[1]), ...]

for i, j in zip(list1, list2): #iterating over 2 list
list(zip(dict1.values(), d2.values())

#enumerate(list): returns enumer obj with [(index, value)]
list = ["apple", "banana"]
list(enumerate(list)) # [(0, "apple"), (1, "banana")]

# all/any
all(list) #if all list elems are truthy returns true
any(list) #if at least one elem is truthy returns true

all(map(lambda x: x> 0, l1)) #if all positive, returns true
```

### DataType methods

```python
#Numbers
bin(20) #0b10100 #turns base 10 value to base 2
hex(20) #0x14 #turns base 10 value to base 16
abs(-4) #absolute value of a number, |-4|
round(4,5) # 4, round(4,6)-> 5
round(3.2229, 3) #3.223
max(3, 4, 10, 7) #10, min(list/tuples or values)
sum(list_or_tuple) #sum of elems(must be nums)
pow(2, 4) #= 2 ** 4

#String
"str".upper() # "str".lower()
"str x".replace(" ", "-") # "str-x"
"str".startswith("s") # "str".endswith("tr")
"str str2".split(" ") # ["str", "str2"] / "-".join(list1)
"xax".strip("x")#"a", rstrip(x="default: space"), lstrip(x)
count(x) #counts string's x values, count(x, startFromIndex)
"lalala".count("a") #3
"str".find("r") #returns first values index or -1
"strs".rfind("s") #starts from end, 3(last s's index)

#List methods
append/pop()/pop(index)/remove(value)/index(value, strtIndx)
list1.extend(list2) #list2 items added/appended to list1
l1.insert(2, "Python") #inserts elem to index
arr1.count(value)/arr1.sort()/arr1.sort(reverse=True)

#Set: unique items, unordered
x = {1, 2, "asd"} #or empty set -> x = set()/set(listOrStr)
list1 = list(x) #to access items convert it to list
x.add(3) #adding item
x.discard(3) #removing/discarding item
set1.difference(set2) #set1.difference_update(set2)
set1.intersection(set2) #common items in both sets
#intersection_update turns set1. into intersection items
set1.isdisjoint(set2) #if there is same item in both-> False
set1.issubset(set2) #returns true if set1 is a sub set of s2
set1.union(set2) # returns a set of both set's items
set1.update(set2) #set1 is updated with union set
```

### SQLITE

```python
import sqlite3 #install dbbrowserforsqlite for browsing data

con = sqlite3.connect("C:/Users/USER/Desktop/sql database/testdb.db")

cursor = con.cursor()

def create_table():
  cursor.execute("""
    CREATE TABLE IF NOT EXISTS books(
      name text,
      writer text,
      publisher text,
      page_count int
    )
  """)
  con.commit()

def insert_data():
  cursor.execute("insert into books values('White Something', 'Jack London?', 'APublisher', 400)")
  con.commit()

def insert_data2():
  title = input("Title: ")
  writer = input("Writer: ")
  publisher = input("Publisher: ")
  number_of_pages = int(input("Number of pages: "))

  cursor.execute("insert into books values(?, ?, ?, ?)", (title, writer, publisher, number_of_pages))
  con.commit() #needed when db updated

def read_all_data():
  cursor.execute("select * from books")
  listData = cursor.fetchall()
  for (title, writer, publisher, pages) in listData:
    print("Title: {}, Writer: {}, page count: {}\n".format(title, writer, pages))

def delete_data(title):
  cursor.execute("delete from books where name=?", (title,))
  con.commit()

def update_db(title, ntitle):
  cursor.execute("update books set name=? where name=?", (ntitle, title))
  con.commit()

update_db("Lord of the rigns", "Lord Of The Rings")

con.close()
```

### *args, *kwargs //\*kwargs: named args, args: nameless

```python
def argsExample(name, *args): #argsE("name", 2, 3, 4)
  print("this is name argument", name)
  for i in args:
    print("rest of args", i)

def kwargsExample(**kwargs): #params will become an obj
  print(kwargs) #kwargsE(name="name", another="something")
  for i,j in kwargs.items(): #name, value
    print(i, j)
```

### Decorator: wraps funcs to add functionality

```python
def calculate_time(func):
  def wrapper(nums):
    start = time.time()
    result = func(nums)#we are storing cuz we need to return
    end = time.time(nums)
    print(func.__name__ + " took" + str(end-start) + "seconds.")
    return result
  return wrapper

@calculate_time
def something(nums):
  result = []
  for i in args:
    resupt.append(i**2)
  return result

something(range(100000))
```

### Iterable: **iter()**, **next()**

```python
# to create an iter obj from iterable obj use iter()
iterable = iter([1, 2, 3]) #print(iterable) #list_iterable object #next(iterable) to get elems on by one

### Creating Iterable Objects: __iter()__ and __next()__ methods
class Asd():
  #def __init__(self, list_of_elems):
  def __iter__(self):
    return self
  def __next__(self):
    self.index += 1 #0 first, starts from -1
    if(self.index < len(self.list_of_elems)):
      return self.list_of_elems[self.index]
    else:
      self.index = -1
      raise StopIteration
```

### Generators: used for not to store all values in memory

```python
def generator_exponent():
  for i in range(1, 6)
    yield i ** 2

gen1 = generator_exponent()
iter1 = iter(gen1)
next(iter1)

list1 = ((i*3) for i in range)
iterator = iter(generator) #next(iterator)

# generator
def anotherGenerator():
  for i in range(1, 11):
    yield "{} x {} = {}".format(i, j, i*j)

for i in anotherGenerator()
  print(i)

#fibonacci with generators
def fib():
  a = 1
  b = 1
  yield a # first it will print 1, 1
  yield b

  while True:
    a, b = b, a + b
    yield b #after that it wil print b(new number)

for i in fib():
  print(i)
  if(i > 1000):
    break
```

### Datetime

```python
from datetime import datetime
locale.setlocale(locale.LC_ALL, "") #to get local month names, "": get from current location


x = datetime.now() #datetime.now().year / month / microseconds / hour
print(datetime.ctime(x.year) #better printing format
print(datetime.strftime(x, "%B %Y"))#%B:month, %D

y = datetime.timestamp(x) #getting timestamp from dt obj
xAgain = datetime.fromtimestamp(y)

dt1 = datetime(2019, 12, 1)
dt2 = datetime.now()
print(dt1 - dt2) #dif between a date and now/days-hours
```

### OS

```python
import os #operation system, dir(os): seeing methods

print(os.getcwd()) #shows current working dir
os.chdir("C:/Users/user/desktop/") #change dir
os.mkdir("name of new dir")
os.mkdirs("/folder/subfolder")
os.rmdir("/afolder/deletethisfolder")
os.rmdirs("/this/that/allwillbedeleted")
os.rename("test.txt", "test2.txt")
os.stat("nameoffile.txt")
for path, dir_name, file_name in os.walk("c://path/etc"):
  print(...) #prints all dirs  at path
```

### SYS

```python
import sys

sys.exit() #exiting program
sys.stdin #getting input from user/system
sys.stdout
sys.stderr.write("error msg") #sys.stderr.flush() #to print instantly-msg goes to buffer-if file is big flush needed
sys.argv #args supplied in commandline
```

### Requests and BeautifulSoup

```python
# pip install requests/beautifulsoup4
import requests
from bs4 import BeautifulSoup

response = requests.get(url)
html_content = response.content

soup = BeautifulSoup(html_content, "html.parser")
print(soup.prettify())
print(soup.find_all("a")) #finding all a tags
for i in soup.find_all("a"): #find_all("div", {"class":"asd"})
  print(i.get("href")) #i.text for getting content
```

### Sending Mail

```python
#https://myaccount.google.com/lesssecureapps - open
#https://realpython.com/python-send-email/ -has html version
import smtplib
import ssl

port = 465  # For SSL
smtp_server = "smtp.gmail.com"
sender_email = "my@gmail.com"  # Enter your address
receiver_email = "your@hotmail.com"  # Enter receiver address
password = input("Type your password and press enter: ")
message = """\
Subject: Hi there

This message is sent from Python.
"""

context = ssl.create_default_context()
with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email, message)

### html version

import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

sender_email = "lordbahlok@gmail.com"
receiver_email = "theeko@hotmail.com"
password = input("Type your password and press enter:")

message = MIMEMultipart("alternative")
message["Subject"] = "multipart test"
message["From"] = sender_email
message["To"] = receiver_email

# Create the plain-text and HTML version of your message
text = """\
Hi,
How are you?utorials:
www.realpython.com"""
html = """\
<html>
  <body>
    <p>Hi,<br>
       How are you?<br>
       <a href="http://www.google.com">google link</a>
    </p>
  </body>
</html>
"""

# Turn these into plain/html MIMEText objects
part1 = MIMEText(text, "plain")
part2 = MIMEText(html, "html")

# Add HTML/plain-text parts to MIMEMultipart message
# The email client will try to render the last part first
message.attach(part1)
message.attach(part2)

# Create secure connection with server and send email
context = ssl.create_default_context()
with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
    server.login(sender_email, password)
    server.sendmail(
        sender_email, receiver_email, message.as_string()
    )
```

### Pillow - Photo Processing

```python
from PIL import Image, ImageFilter

img = Image.open("C:/Users/USER/Desktop/img1.jpg")
#img.show()
#img.save("img11.jpg")
#img.rotate(180).save("img12.jpg")
#img.convert(mode = "L").show() #or save
# newSize = (100, 100)
# img.thumbnail(newSize)
# img.save("img14.jpg")
# img.filter(ImageFilter.GaussianBlur(15)).save("img17.jpg")
# cropCoords = (340, 0, 950, 600)
# img.crop(cropCoords).save("newname.jpg")
```

### PyQt5 : for gui apps

```python
# jupyter notebook -> shift enter: run,
#stringVar. then tab to see available methods
#stringVar.cap + tab -> capitalize
#stringVar.methodName + shift + tab = description of method
#help(strVar) / help(strVar.method) : to get help for method
```
