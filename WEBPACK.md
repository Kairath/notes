## WEBPACK

```js
// it loads/bundles js, files(img, etc)
{"build": "webpack" }// or npx webpack in console
npm run build -- --config webpack.config.js // -- before custom npm script to pass custom param

//webpack.config.js
 const path = require('path');

  module.exports = {
    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: path.join(__dirname, 'dist')
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"]
        }
      ]
    }
  };

```
