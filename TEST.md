#Testing - Mocha(Testing Framework) / Chai (Testing Library)
Suite, describe, should etc Mocha. Assert is part of Chai
Jest is another option for testing lib, can be used with Mocha. Has snapshot testing feature.
Sinon is used for mock, spy etc.
Enzym is used for testing react components.

"test": "mocha **/*.test.js" //test script
nodemon --exec "npm test" //auto running script with nodemon
"test-watch": "nodemon --exec \"npm test\"" //npm run test-watch(custom npm script-> run)

#####Dependencies
```
"mocha": "^3.1.0",
"chai": "^3.5.0",
"chai-http": "^3.0.0",
```

##### BDD: Behavior Driven Development

specification(spec)(3 parts): describe("title", function() { ... })

```js
describe("pow", function() {

  it("raises to n-th power", function() {
    assert.equal(pow(2, 3), 8);
  });

});
```

####Testing - Tdd style
suite("description", function(){}): describe tests
//since analogs are not quite right before, beforeEach etc can be used instead of these
suiteSetup():analogous to before
suiteTeardown(): analogous to after
teardown(): analogous to afterEach
setup(): analogous to beforeEach


```js
var chai = require "chai";
var assert = chai.assert;

setup(function(){
  //this should run before suites
  //should act like befor()
})

suite("what is suite/test is for // or use describe and beforeEach etc", function(){

  suite("Basic Assertions", function(){

    suiteSetup(function(){
      //like beforeEach()
      //before each test this should run
    })

    test("whatever test is", function(){
      assert.isNull/isNotNull(null, "optional description");

      //isDefined, isUndefined
      //isOk-isNotOk: truthy, falsey
      //isTrue-isNotTrue (2args)
      //equal-notEqual(==) (3 args)
      //strictEqual-notStrictEqual(===)
      //
    })

    function someFuncOutsideASutie(){...}

    suite("Desc of test", function(){
      //isAtMost(<=), isAbove(>) //isAtLeast(>=), isBelow(<)

      assert.approximately(actual, expected, range, "[optional msg]");

    });

    suite("Arrays", function(){
      //isArray-isNotArray,include-notInclude
    });

    suite("Strings", function(){
      //isString-isNotString, include-notInclude
      //match("str", regex) - notMatch(str, regex)
    });

    suite("Object", function(){
      //property-notProperty, typeOf-notTypeOf
      //instanceOf-notInstanceOf
    });

  });

});
```

###Async Testing
```
var chai = require('chai');
var assert = chai.assert;

var server = require('../server'); //express app-app from server.js

var chaiHttp = require('chai-http'); //chai http plugin
chai.use(chaiHttp);     

suite('Functional Tests', function() {

  test("Asynch test example", function(done){

    setTimeoft(function(){
      assert.isOk("Async test");
      done(); // call done after async operation is complete

    }, 500); // this will run after 500ms

  });

  suite('GET /hello?name=[name] => "hello [name]"', function(){

      test('#example - ?name=John',  function(done){   // Don't forget the callback...
         chai.request(server)             // 'server' is the Express App
          .get('/hello?name=John')        // http_method(url)
          .end(function(err, res){        // Send req. Pass a callback in node style.

            assert.equal(res.status, 200, 'response status should be 200');
            // res.text contains the response as a string
            assert.equal(res.text, 'hello John', 'response should be "hello John"');
            done();
          });
      });

      suite('PUT /travellers', function(){
      test('#example - responds with appropriate JSON data when sending {surname: "Polo"}',  function(done){
         chai.request(server)
          .put('/travellers')
          .send({surname: 'Polo'})    // attach the payload, encoded as JSON
          .end(function(err, res){    // Send the request. Pass a Node callback

            assert.equal(res.status, 200, 'response status should be 200');
            assert.equal(res.type, 'application/json', "Response should be json");
            
            // res.body contains the response parsed as a JS object, when appropriate
            // (i.e the response type is JSON)
            assert.equal(res.body.name, 'Marco', 'res.body.name should be "Marco"');
            assert.equal(res.body.surname, 'Polo', 'res.body.surname should be "Polo"' );
            
            // call 'done()' when... done
            done();
          });
      });

});

```

###Zombie.js Headless Browser for testing 
Headless Browser = no GUI
```js
var Browser = require('zombie');

Browser.site = 'https://mocha-chai-testing.glitch.me'; 

// If you are testing on a local environment replace the line above  with 
// Browser.localhost('example.com', (process.env.PORT || 3000));

suite('e2e Testing with Zombie.js', function() {
  const browser = new Browser();

  // With a headless browser, before the actual testing, we need to
  // **visit** the page we are going to inspect...

  suiteSetup(function(done) { // Remember, web interactions are asynchronous !!
    return browser.visit('/', done);  // Browser asynchronous operations take a callback
  });

  suite('"Famous Italian Explorers" form', function() {
    
    
    test('#example - submit the input "surname" : "Polo"', function(done) {
      browser
        .fill('surname', 'Polo')
        .pressButton('submit', function(){
          // pressButton is ## Async ##.  
          // It waits for the ajax call to complete...

          // assert that status is OK 200
          browser.assert.success();
          // assert that the text inside the element 'span#name' is 'Marco'
          browser.assert.text('span#name', 'Marco');
          // assert that the text inside the element 'span#surname' is 'Polo'
          browser.assert.text('span#surname', 'Polo');
          // assert that the element(s) 'span#dates' exist and their count is 1
          browser.assert.element('span#dates', 1);

          done();   // It's an async test, so we have to call 'done()''
        });
    });

 
  });
});
```


```js
const chai = require("chai);
const assert = chai.assert;

describe("desc of test", ()=>{
  it("describe test case here", ()=>{
    assert(2).to.equal(2);
  });

  describe("another nested test",()=>{
    it("async test", (done)=>{ //add done here
      utils.asyncAdd(a, (result)=>{
        expect(result).to.be(b);
        done();
      })
    })
  })
});
```

```js
var express = require("express");

var app = express();
app.get("/users", (req, res)=>res.status(200).json({name: "asd"}));


 // npm i --save-dev supertest to test express apps
var request = require("supertest");
var app = require("./server").app; //app exported in server module

it("should return hello response", (done)=>{
  request(app)
    .get("/")
    .expect(200)
    .expect("hello /")
    .expect((res)=>{ //res object
      expect(res.body).to.include(...) // mocha etc can be used like this
    })
    .end(done);
});
```