- variable names cant be started with nums, operators
- only vars are case sensitive // Echo == echo

"$asd" can be used as "{$asd}" // for advance operations // "{$asd->foo}"

echo $numVar++ //writes first then changes var

echo ++$numVar //changes var first then writes output

double and single quote difference: single takes all as string

```
// cmd
php -h // help
php filename.php
php -S http://localhost:8888 // php built in development server

htmlspecialchars($_GET["name"]); // html codes converted to string
```

- require(""); // gives error if it cant find file
- include(""); // gives warning if file not found

```

$array = ["first elem", 2, "third"]; // array() -> shortcut []

define("constantName", "constantValue");

echo __FILE__  #current place

echo __LINE__  #current line

switch($varName){
    case "a":
        #code
        break;
    default:
        #code
        break;
}

for($i=0; $i<10; $i++){ #code } 

do {} while(cond) {}

foreach ( $a as $key => $val ){ } // or foreach ( $a as $b ) { }
// alternative syntax
<?php foreach(): ?>
    #html
<?php endforeach; ?>

```

```
$arr = ["a", "b"];
$arr[] = "c"; // now $arr = ["a", "b", "c"]
unset($arr["b"]); // now $arr = ["a", c]
$nowString = implode(",", $arr); // now its a string: a, c
explode("," $nowString); // array again: ["a", "c"]
```

```
method_exists("className", "methodName"); // returns true if method exists
```

```
var_dump($var); // used for learning a variable content and type
die(var_dump($var)); // or var_dump($var); die(); // die, exit: same thing
```

##### ob_start
used if header(redirecting) used or if u dont want output till the end

##### session_start(); // for using/accesing session

##### if(isset($_SESSION["user"]){ header("Location:login.php"); }

##### if form action not set its sends to same page


##### ?asd=123 => for GET data $_GET["asd"] used

#### --- DESTROYING SESSION ---
```
session_start();
session_destroy();
header("Location:login.php");
```
##### md5($var) => for crypting data via md5 lib

##### SQL EXAMPLES
```
SELECT * FROM t_name WHERE a=b;

UPDATE t_name SET a=x, b=y WHERE id=$id;

INSERT INTO t_name (a, b, c) VALUES("$x", "$y", "$z");

DELETE FROM t_name WHERE id=$id;

SELECT * FROM orders_table INNER JOIN customers_table 
ON orders_table.customer_id=customer_table.customer_id;

SELECT customer.customerName, Orders.OrderId 
FROM Customers LEFT JOIN Orders 
ON Customers.CustomerId = Orders.CustomerId 
ORDER BY ...
```


##### SENDING FILE FROM FORM

form *enctype="multipart/form-data"*
```
if(isset($_POST["nameOfSubmit"]){
    @upload_dir = "../uploads"; //3 for substr ../
    @tmp_name = $_FILE["fileInputName"]["tmp_name"];
    @name = $_FILE["fileInputName"]["name"];
    
    $uniqueName = rand(20000,30000).rand(20000,30000);
    $imgRef = substr($upload_dir, 3).$uniqueName.$name;
    
    @move_uploaded_file($tmp_name, "$uploads_dir/$uniqueName$name");
    
    //mysql query for info
    //if mysql affected x or b
}
```
###### unlink("filename"); //file deleting

###### error_reporting(0); //turning reporting off

###### $time = date('Y-M-d H:i');

##### putenv("asd") = "hmm"; getenv("asd"); // ENV VARS

##### ALTER TABLE t_name AUTO_INCREMENT=1; // reseting auto increment


#### SITE MAP CODES

```
<urlset>
    <url>
        <loc>http://asd/iletisim</loc>
        <changefreq>daily</changefreq>
        <priority>1.00</priority>
    </url>
```

#### PDO // for activating php.ini => php.pdo.dll
```
---connect.php
try{
 $db = new PDO("mysql:host=localhost;dbname=nameofdb;charset=utf8", $username, $password);
} catch(Exception $e){
    echo $e->getMessage();
}

---cmd commands // or use phpMyAdmin or a gui for db management
mysql -u root -p
show databases;
create database mydb;
use mydb;
create table todos (id int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
    description text NOT NULL, completed bolean NOT NULL );
drop table todos;
describe todos; (optional - to see your table information)
insert into todos (description, completed) values('Go to the store', false);
select * from todos; (optional - to select everything from that row in order to see its information)
```

$db->lastInsertedid(); // last inserted recs id

```
$query = $db->prepare("SELECT ... where id=:id);
$records = $query->execute(array("id"=>$id));
while($rec = $record->fetch(PDO::FETCH_ASSOC){ #code }
// or PDO::FETCH_OBJ for general object
// or (PDO::FETCH_CLASS, 'Task') // fetches data as Task class
// for that we need Task class to be defined

class Task{
    private decsription;
    private completed;
    
    public function nowFetchedDataCanCallThisMethod(){
        return "foobar";
}

// now fetched data can call the func cuz its a class instance
tasks = $statement->fetchAll(PDO::FETCH_CLASS, "Task");
var_dump($tasks[0]->nowFetchedDataCanCallThisMethod());
```


```
//search query
->prepare("select ... where asd LIKE '%searched%");
```

#### PAGINATION

```
$atPage = 4;

$query = $db->prepare("select...");
$content = $query->execute();
$total_content = $content->rowCount();

$totalPage = ceil(total_content/$atPage);

if($page<1) $sayfa = 1;
if($page > totalPage) $page = $totalPage;
//limit var
//query with limit
```
##### adress:2082 // cpanel shortcut

#### $_SERVER
```
$_SERVER["REMOTE_ADDR"]; // ip of client
$_SERVER["SERVER_ADDR"]; // ip of server
$_SERVER["HTTP_USER_AGENT"]; // users web browser info

$_SERVER["HTTP_HOST"]; // url: www.asd.com
$_SERVER["REQUEST_URI"]; // uri: /asd
```
#### ob_start callback func will run after ob_end_flush
```
function callBackForOb($tampon){ 
    echo "what u were keeping in mind: $tampon";
    echo "this callback will run after ob_end"
}
ob_start("callBackForOb");

// can be used after end_clean()
$whatKeptInSilent = ob_get_content(); 

ob_end_flush(); // ob_end_clean(); for no output
```


##### date_default_timezone_set('Europe/Istanbul'); // for server time problem

##### http://www.dropzonejs.com/  FOR MULTI IMAGE UPLOAD
```
//in html
<form action="/file-upload" class="dropzone">
    // with a hidden input, id can also be sent
  <div class="fallback">  // fallback not mandatory
    <input name="file" type="file" multiple />
  </div>
</form>

//in php file aka file-upload file u specified at form action attr
if(!empty($_FILES){
    // standart code for saving it to db. dropzone sends 1 by 1
}
```

#### GOOGLE MAP API // there is a api key for local
```
<div id="googlemaps"...
    <iframe widath="100%" height="100%" frameborder="0"
        style="border:0"
        src="https://www.google.com/
            maps/embed/u1/place
            ?key=<?php echo $settings['g_map_key'] ?>
            &q=<?php echo $settings['adress'] ?>"
            allowfullscreen>
    </iframe>
</div> // div used for css styles
```

#### BOT / WEB SCRAPPING
```
set_time_limit(0); // server waits response no matter how long
error_reporting(E_ALL ^ E_NOTICE); 

file_get_contents("urlOrFile"); // gets files contents

strip_tags($foo); // cleans html tags

$pattern = "/regexStuf/"

preg_match_all($pattern, $subject, $macthes);



$a = file_gets_contents($url);
$open = fopen('img/product'.$uniqueName.$name, 'w+');
fwrite($open, $a);
fclose($open);

```

***

```
header("refresh:10;$siteUrl"); //redirects after 10 sec

```

##### footer locking // 1 way is checking size

```
$footer = "footer.php";
$footerSize = filesize($footer);

if($footerSize != 3599){ //first size were 3599
    #code
}

```
##### IonCube for encrypt files
##### Cpanel -> cronjobs or cron-job.org  // auto file execution

