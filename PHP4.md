```
class Connection
{

  function make()
  {
    try {
    
      // or create a config.php file : returns assoc array
      // make($config) // will be called config.php["database"];
      // and get username etc from that passed assoc array
      return new PDO("mysql:host=localhost;dbname=testinglara;charset=utf8", "username", "pass");
    } catch (Exception $e) {
      die($e->getMessage());
    }
  }
}

class QueryBuilder
{

  protected $db;

  public function __construct(PDO $db){
    $this->db = $db;
  }

  public function selectAll($table)
  {
    $todos = $this->db->prepare("select * from {$table}");
    $todos->execute();

    $className = ucfirst(substr($table, 0, -1));

    return $todos->fetchAll(PDO::FETCH_CLASS, "$className");
  }
}

class Todo{
  public $description;
  public $completed;

  public function fooBar()
  {
    return "fooBar";
  }
}


$db = Connection::make();
$query = new QueryBuilder($db);
//or just $query = new QueryBuilder(Connection::make());

// all prev code can be moved to bootstrap folder then
// at the end return new QueryBuilder(Connection::make());
// and in file which uses query,
// we can just say $query = require("bootstrap.php");

$results = $query->selectAll("todos");

dd($results);

```

##### for creating a instance in static method
- use new static, or new self
- in static methods $this cant be used, static methods not instance methods
- accesing static props: $this cant be used even if a public method accesing it
- again it must be called as: static::$prop
- accesing a static prop from outside-> Person::$prop 
- so :: can acces static (public, private, protected) props, constants and static methods
- static props are shared
```
public static blabla(){
$newInstance = new static;
//and if u wanna acces a protected prop, method etc and return
return $newInstance->propName;
}
```

##### accesing static prop from static method
```
class A{
  protected static $prop;
  protected $anotherProp;
  
  public static aStaticMethod($arg){
    static::$prop = $arg;
  }
  
  // we need that cause we cant acces static property, 
  // it must be constant or func
  // also its protected
  public static getterForStaticProp(){
    return static::$prop;
  }
}

A::("value"); // for setting the prop
var_dump(A::getterForStaticProp()); // for getting the prop

// if later a instance created: new A(), $prop of that instance will be null
// but non instance $prop val stil can be accesed, its stored as scope variable i guess

// accesing a property from static or self -> static::$propName;
// accesing a property from $this (or class if its public) -> $this->propName or (new A)->propName
```

```
// trim takes extra arg -> it will be removed if arg is at the start or end
trim($thatWillbeTrimmed, "/"); 
```

##### php -S localhost:8888 // php development server // cmd command

- php -S localhost:8888 // if file asked like localhost:8888/foo.php -> file returns
- // if localhost/something requested ->
- taking uri and showing that page index.php's repsonsibility
- php -S localhost:8000 -t foo/ # now document root is where command fired + "/foo/"
- php -S localhost:8888 router.php // now sends every req to router.php


##### Composer Autoload
```
---composer.json
{
  {
    "autoload": {
    
      "classmap": [
            "./" // for auto loading all classes
        ],
        "psr-4": {  // psr-4 used standart in industry for autoload
            "App\\": "app/" 
        }
    }
  }
}

//and after composer.json file 
composer install
//there is info files in composer/ folder
//in entry point of your app
require "vendor/autoload.php";

```

- compact("name") -> means [ "user" => $user ] // php func
- extract does opposite of compact -> if there is a $data = ["user" => "value"] 
- extract($data) // creates: $user = "value"; automatically

- use App\Core\{Router, Request} // php7: can be used for multiple classes

##### Abstract Class // Cant be instantained
```
abstract class myC{ // instance cant be created if its a abstract class
  // if there is a abs func, class must be abstract
  // abstract func cant define implementation, just defines method's signature
  abstract public function myFunc(); 
}

```

##### Interface // can only define public methods 
// for protected methods-> abstract classes must be used
// multiple implementation supported -> implements A, B
// multiple inheritance not supported
```
interface Logger {
  public function execute($msg);
}

class LogtoFile implement Logger{
  
  public function execute($msg) {
    var_dump("log to message to a file: ". $msg);
  }
}

class LogToDatabase implements Logger{
  
  public function execute($msg){
    var_dump("log to message to a database: ". $msg);
  }
}

// 
class UserController{
  $protected $logger;
  
  // instead of saying __construct(LogToFile $logger), wanting a concrete class
  public function __contruct(Logger $logger){ // the passed arg must have a execute method
    $this->logger = $logger;
  }
  
  public function show(){
    $this->logger->execute();
  }
}

$controller = new UserController(new LogToFile);
$controller->show();
```

```
usort($arr, function($a, $b) use $anotherVariable { 
  return $a <=> $b;
}

```

##### PHP7 NEW STUFF
-scalar type hinting works with php 5.6 (type hinting for basic classes: string, array)
```
// return types can be declared
function aFunc(): array {} // returns an array
// spaceship operator // if used with usort() func, by default smaller ones will be listed at top
1 <=> 2 // -1 0 1, if left is smaller: -1, equal: 0, right side one smaller: 1

// null collapse operator (??) - if the value truthy uses that, if no second stuff will be used
$foo = $bar ?? "if bar not true, this will be foo's value";

// grouped import // composer.json, classes, packages, require autoloader...
use App\{MyClass1, myClass2} // will use/import/require App\MyClass1 and use App\MyClass2

use App{
  MyClass1,
  ASD\MyClass2 // that one means App\ASD\Myclass2
}

// Anonymous Classes
new class { #code }
new class implements ABC { #code } // can use same keywords

```

- alias nah="git reset --hard;git clean -df;"