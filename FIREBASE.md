# Firebase

### Firestore: a noSQL database: collection of documents

```js
// npm i firebase // src/config/fbConfig.js
import firebase from "firebase/app" // /app: only base features
import "firebase/firestore" // db-firestore
import "firebase/auth" // authentication
let firebaseConfig = {...}
firebase(firebaseConfig);
firebase.firestore().settings({timestampsInSnapshots: true}) // db

export {firebase.auth() as auth, firebase.firestore() as db}
export default firebase;


// To host your app on firabase and
//to run a local web server for development to access some features
npm install -g firebase-tools
firebase login
firebase init // on root of doc


//applyMiddleware(thunk.withExtraArgument({ stuff }))
// now thunk calls with (dispatch, getStore, {stuff})

firestore.collection("projects") // collectionName
      .add({  ..., createdAt: new Date() })
      .then(result => { // returns new docs id
        dispatch({
          type: CREATE_PROJECT,
          payload: project.data()
        });
      })
      .catch(e => {
        dispatch({ type: "CREATE_PROJECT_ERROR", payload: e });
      });
  };
};

// FIREBASE

//npm install firebase
import firebase from "firebase/app";
require("firebase/firestore"); //db

firebase.initializeApp({
  apiKey: '### FIREBASE API KEY ###',
  authDomain: '### FIREBASE AUTH DOMAIN ###',
  projectId: '### CLOUD FIRESTORE PROJECT ID ###'
});

export const db = firebase.firestore(); // pass to withExtraArguments if it is a redux app
export const auth = firebase.auth();
export default firebase;

//adding data
db.collection("users").add({
    first: "Ada",
    last: "Lovelace",
    born: 1815
})
.then(function(docRef) {
    console.log("Document written with ID: ", docRef.id);
})
.catch(function(error) {
    console.error("Error adding document: ", error);
});

//reading data
db.collection("users").get().then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
    });
});

// getting all docs from a collection
export const fetchAllProducts = () => (
  dispatch,
  getState,
  { firabase, db } // or import db from fbConfig.js file instead of using extraArgs of thunk
) => {
  dispatch({ type: FIREBASE_FETCHING_START });
  return db
    .collection("projects")
    .get()
    .then(querySnapshot => {
      let result = [];
      querySnapshot.forEach(doc => {
        let data = doc.data();
        let id = doc.id;
        result.push({ id, ...data });
      });

      dispatch({
        type: GET_ALL_PROJECTS_COLLECTION,
        payload: result
      });
    });
};

// auth
// new user
firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
  var errorCode = error.code;
  var errorMessage = error.message;
});

//login existing user
firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
  var errorCode = error.code;
  var errorMessage = error.message;
});

//logout
firebase.auth().signOut().then(()=> {}).catch(function(error) {});

// login-logout observer : called when auth state changes
firebase.auth().onAuthStateChanged(function(user) {
  if (user) { // User is signed in.
    const {displayName, email, emailVerified, photoURL, isAnonymous, uid, providerData} = user;
    // or dispatch auth_login action
  } else { // User is signed out.// ... }
});

//current user
var user = firebase.auth().currentUser;

//updating user profile
user.updateProfile({
  displayName: "Jane Q. User",
  photoURL: "https://example.com/jane-q-user/profile.jpg"
}).then(function() {  Update successful. }).catch(function(error) {
  // An error happened.
});

//verification mail
var user = firebase.auth().currentUser;

user.sendEmailVerification().then(function() {Email sent}).catch(function(error) {
  // An error happened.
});

//password reset
var auth = firebase.auth();
var emailAddress = "user@example.com";

auth.sendPasswordResetEmail(emailAddress).then(()=> /*sent*/)
.catch(err) => /* An error happened.*/});

//delete user
user.delete().then(function() {
  // User deleted.
}).catch(function(error) {
  // An error happened.
});

// creating a user doc when signup/auth.createUser called

export const signUpUser = ({ email, password, firstName, lastName }) => (
  dispatch,
  getState,
  { auth, db }
) => {
  auth
    .createUserWithEmailAndPassword(email, password)
    .db(docRef => {
      return db
        .collection("users") // if no collection fb will create new.
        .doc(docRef.user.uid) // referencing a doc with id / add auto creates id
        .set({ //doc method creates a doc with given id and
          firstName, // with set fields are set
          lastName
        });
    })
    .then(() => {
      dispatch({ type: AUTH_SIGNUP_SUCCESS });
    })
    .catch(function(error) {
      dispatch({ type: AUTH_SIGNUP_ERROR, payload: error });
    });
};

//getting profile data from users doc with user.uid
function getCurrentUserProfile() {
  let user = auth.currentUser;

  if (!user) {
    store.dispatch({ type: AUTH_SET_PROFILE, payload: null });
  } else {
    db.collection("users")
      .doc(user.uid)
      .get()
      .then(doc => {
        store.dispatch({ type: AUTH_SET_PROFILE, payload: doc.data() });
      });
  }
}

//displaying created at
// moment or project.createdAt.toDate().toString()
{moment(project.createdAt.toDate()).calendar()}


// rule
service cloud.firestore {
  match /databases/{database}/documents {
    match /projects/{project} { // a single project = {project}
      allow read, write: if request.auth.uid != null;// if uid not null
      //allow method : rule
    }
    match /users/{userId} { // a single project = {project}
      allow create // anyone can create a user
      allow read: if request.auth.uid != null //logged in can read
      allow write: if request.auth.uid = userId//if user id equal
    }
  }
}
```

### FUNCTIONS

```js
//functions-getstarted
//npm install -g firebase-tools
//firebase init //dist for directory
//no need to init for firebase or auth, rules for them in fb already
//functions/index.js where we are gonna declare fb functions
//functions/index.js
exports.helloWorld = functions.https.onRequest((request, response) => {
  response.send("Hello from Firebase!");
});
//firebase deploy // deploys entire app - npm run build first
//firebase deploy --only functions // only deploys functions/

// notification function
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);

const createNotification = notification => {
  return admin
    .firestore()
    .collection("notifications")
    .add(notification)
    .then(doc => console.log("notification added", doc));
};

exports.projectCreated = functions.firestore
  .document(`projects/{projectId}`)
  .onCreate(doc => {
    const project = doc.data();
    const notification = {
      content: "Added a new project",
      user: `${project.authorFirstName} ${project.authorLastName}`,
      time: admin.firestore.FieldValue.serverTimestamp()
    };

    return createNotification(notification);
  });

//creating notification doc on user sign up
exports.userJoined = functions.auth.user().onCreate(user => {
  return admin
    .firestore()
    .collection("users")
    .doc(user.uid)
    .get()
    .then(doc => {
      const newUser = doc.data();
      const notification = {
        content: "Joined the party",
        user: `${newUser.firstName} ${newUser.lastName}`,
        time: admin.firestore.FieldValue.serverTimestamp()
      };

      return createNotification(notification);
    });
});
```
