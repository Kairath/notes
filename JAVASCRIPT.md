# JAVASCRIPT

```js
<script src="path/to/script"> // code here will be omitted </script>
<script>
  // without src attr, code here will be executed
  // only comment can come before use strict
  "use strict";  // switches engine to modern mode
</script>

// oneline comment /* multiline comment */
// variable names must start with chars, cant start with num, $ and _ can be used, case matters in js

// typeof x or typeof(x) to get type of something

// Type Conversation
String(3) // turns it to a string
Number("4") // turns to number 4. undefined becomes nan, null 0, true/false 1 and 0, strings if empty(gets rid of spaces) 0 else NaN
// +: if used as unary operator it turns variable to number. +varName
Bolean("0") //turns into bolean, intuitively empty values become false(0, null, undefined, "", NaN), others true

// Falsy values: false, null, undefined, NaN, 0, '', "", ``(empty string)

// operand(what operators applied to), unary(single operators like +varName), binary(operators that has 2 operand) // unary has higher precedence

varName++ //returns prev var value, increases 1, ++varName returns next value

// asigment operator = also returns value
// **: exponentiation, %: remainder operator
// modify and assign operators: n *= 2; (n = n * 2)

//Comparasions (for string dictionary-lexicopgraphical order is used, a is greater(has higher unicode num) than A)
// String comparation: localization lib Intl.JS helper function for language based comparation
`alert( 'Österreich'.localeCompare('Zealand') ); // -1` // left is greater, 0 equal, 1 right is greater
`str.trim() – removes (“trims”) spaces from the beginning and end`
`str.repeat(n); str.includes("asd"); str.startsWith/endsWith("asd")`
`str.slice(start, endNotIncluded); substr(start, length);`

`array.splice(index[, deleteCount, elem1, ..., elemN]);` // delete arr[index] removes since arr is an obj but value becomes undefined
`[1, 2, 5].splice(-1, 0, 3, 4);` // now it is 1,2,3,4,5
// arr1.concat(itemsOrArrays); //indexOf/lastIndexOf(item, pos);
// arr.includes(value); // arr.find/filter(funct): returns first/all matches //arr.findIndex
// split(delimeter) : string to array // arr.join(delimeter): array to string // arr.copyWithin(target, start, end) ;
// arr.some(fn); arr.every(fn) : returns true/false if one/all returns/meets criteria
`arr.map(funct); arr.sort((a,b)=> a-b); arr.reverse(); Array.isArray(arr);`
`array.fill(fillValue, startIndex, end);  let arr = new Arr(10).fill("hmm"); // creates a 10 elem array and fills values with hmm`

// if different types compares they are converted to numbers
// > < >= <= != ==  === !==

//Logical operators: || (OR), && (AND), ! (NOT).
result = value1 || value2 || value3; // short circuit
// OR checks values' boolean val if true returns orginal val if not goes from left to right one by one, if none true returns last value
let result = value1 && value2 // will return/asign first false or last value, has higher precedence than or
!result // not logical operator converts value to bolean and returns inverse
!!result // used for converting values to boolean equavalent (boolean, reverse, reverse)(! has highest precedence of logical operators)

`Value at coords (${i},${j})` // using variables in a string

school.hasOwnProperty ("schoolName") // returns true if obj has property

// if {} used with arrow func, return statement needed, no auto return
let asd = (a) => a*a // if one line {} can be omited, if only one arg () can be omited, if one line arrow func returns auto

//Alert, Promt, Confirm: User interactions(Browser Specific Functions)
alert(message); let result = prompt(title[, default]); let isBoss = confirm("Are you the boss?"); /*ok true, cancel false */

//The comma operator allows us to evaluate several expressions. Each of them is evaluated, but the result of only the last one is returned.
let a = (1 + 2, 3 + 4); alert( a ); // 7 (the result of 3 + 4); for (a = 1, b = 3, c = a * b; a < 10; a++) {...}

for(var item in objName) { /* code */ }
for(let item of objName) { /* code */ }

JSON.stringify(target); //turns json obj to string // has other variation/args
JSON.parse(target); //turning/parsing string to a json obj

// IIFE (Immediately Invoked Function Expression)
(function(){
  //do some work // functions default return value is undefined same as return;
})();

(function(window, $, undefined){
  //do some work
})(window, jQuery);

let arr = [1, 2, 3, 4, 5]; // arr.length = 2; -> arr now [1, 2, 3] // irreversible // arr.length = 0; simplest way to clear
let arr = new Array("Apple", "Pear"); // second syntax, new Array(2); creates an array with 2 undefined elements, length 2

//arguments is a special array like iterable, keeps track of all args func showName(){ arguments.length} // arrow funcs has no arguments
function sumAll(...args){} // args will be all arguments as array-> for(let arg of args) sum += arg;
function showName(firstName, lastName, ...titles) {} // rest goes to titles array

// if there is a return in try catch->it will return will come after finally block
try { /*code we can throw new Error('error msg') */ } catch(error) { /*error has error.name and error. message
*/} finally { /*executed always after try-catch */}
// node has process.on(‘uncaughtException’), browser has window.onerror = function(message, url, line, col, error) {} //runs if uncaught error

// The "pseudocode" for the built-in Error class defined by JavaScript itself
class Error {
  constructor(message) {
    this.message = message;
    this.name = "Error"; // (different names for different built-in error classes)
    this.stack = <nested calls>; // non-standard, but most environments support it
  }
}

class ValidationError extends Error {
  constructor(message) {
    super(message); // (1)
    this.name = "ValidationError"; // (2)
  }
}

throw new ValidationError("msg");
```

#### Conditionals, Loops

```js
if ( condition )  { code; } else if { code; } else { code }

//ternary” or “question mark” operator
let message = (age < 3) ? 'Hi, baby!' : (age < 18) ? 'Hello!' : (age < 100) ? 'Greetings!' : 'What an unusual age!';

switch (/*Some expression*/) {
    case 'option1': //option and expression must be same type(string string etc)/ checks are always strict equal(===)
        // Do something
        break;
    case 'option2':
    case 'option3':
        // if matches opt2 goes till here cause no break
        break;
    default:
       // Do yet another thing
}

// (name, age="default value can be asigned like this")
// old browser has no def param support -> if(argi === undefined) { argi = "default" } or (){ argi == argi || "default"}
var areaBox = function(length, width) { //or function areaBox(){} //func expression, func declaration
    return length * width; // func experessions cant be called before creation unlike func declaration(func _(){})
}; // or es6 arrow func: new areaBox = (l, w) => l * w

// break; used for breaking/force exiting a loop // break-cont not works with ternay operator(?)
for (var i = 1; i < 11; i = i + 1) { // code } // for(;i<11;){ //all parts can be skipped}
labelName: for(...) { for(...){ if(condition){break labelName} }} //label used for breaking outer(both) loop // or labelName: /newline for
while(condition){ // Do something! if something break; }
do { /*code; if(something) { continue; continue skips current iteration and starts new one if condition allows*/ } while (loopCondition);

//Closures
function makeAdder(a) {
  return function(b) { //func inside a func creates a closure(another scope)
    return a + b;
  }
}

var x = makeAdder(5); //x(6); // returns 11
var y = makeAdder(20); y(7); // returns 27
```

### Map, Set, Weakmap, Weakset

```js
//Map: unlike obj anything can be key, even object can be key

let map = new Map();
map.set(keyName, value); // set can be chained set(a, b).set(c, d);
map.get(keyName);
map.size // 1

// when map is created an array (or another iterable) with key-val pairs can be passed
let map = new Map([ ['1',  'str1'], [1,    'num1'] ]); //map.get(1) -> 'num1'

// Object.entries converts an object to an array of key value pairs
let map = new Map(Object.entries({
  name: "John",
  age: 30
}));

// iterating over map
map.keys() // returns an iterable for keys,
map.values() // returns an iterable for values,
map.entries() // returns an iterable for entries [key, value], it’s used by default in for..of.

for (let vegetable of recipeMap.keys()) { // of recepeMap -> def->entries
  alert(vegetable); //[key, value] if no .key() or .values()
}

// Map has built-in forEach loop, similar to Array:
recipeMap.forEach( (value, key, map) => {
  alert(`${key}: ${value}`); // cucumber: 500 etc
});

// Set:  collection of values, where each value may occur only once.

new Set(iterable) – creates the set, optionally from an array of values (any iterable will do).
set.add(value) // adds a value, returns the set itself.
set.delete(value) // removes the value, returns true if value existed at the moment of the call, otherwise false.
set.has(value) // returns true if the value exists in the set, otherwise false.
set.clear() // removes everything from the set.
set.size // is the elements count.

let set = new Set();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

// visits, some users come multiple times
set.add(john);
set.add(pete);
set.add(mary);
set.add(john);
set.add(mary);

// set keeps only unique values
alert( set.size ); // 3

for (let user of set) {
  alert(user.name); // John (then Pete and Mary)
}

// Iteration over Set
// We can loop over a set either with for..of or using forEach:
let set = new Set(["oranges", "apples", "bananas"]);

for (let value of set) alert(value);

// the same with forEach:
set.forEach((value, valueAgain, set) => {
  alert(value);

/*
The same methods Map has for iterators are also supported:

set.keys() – returns an iterable object for values,
set.values() – same as set.keys, for compatibility with Map,
set.entries() – returns an iterable object for entries [value, value], exists for compatibility with Map.
});
*/

// WeakMap and WeakSet
// WeakSet is a special kind of Set that does not prevent JavaScript from removing its items from memory. WeakMap is the same thing for Map.

// WeakMap keys must be obj, not primitive values
// has no support for iteration and methods
// only has .get(key), .set(key, value), .delete(key, value), .has(key) methods
// Now, if we use an object as the key in it, and there are no other references to that object – it will be removed from memory (and from the map) automatically.
let john = { name: "John" };
let weakMap = new WeakMap();
weakMap.set(john, "...");

john = null; // overwrite the reference // john is removed from memory!

// WeakSet: only obj key, only exist when there is a ref, only supports add, has, delete but not size and keys and no iterations

//uri encoding
encodeURIComponent("asd asd"); // asd%20asd
decodeURIComponent("asd%20asd") // asd asd

```

### Object.key, values, entries : supported for map, set, array

```js
Object.keys(user) = ["name", "age"]
Object.values(user) = ["John", 30]
Object.entries(user) = [ ["name","John"], ["age",30] ]

for (let value of Object.values(user)) {
  alert(value); // John, then 30
}
```

#### Date and time

```js
let now = new Date(); // shows current date/time
new Date(millisecondsAkaTimestamp); // 0 means 01.01.1970 UTC+0
let date = new Date("2017-01-26"); // new Date(datestring)

//new Date(year, month, date, hours, minutes, seconds, ms)
//2 is req:4digit, 0-11,dayofmonth(1 default), 0 default(h, m, s, ms)
let date = new Date(2011, 0, 1, 2, 3, 4, 567); // 1.01.2011, 02:03:04.567

// Access date components
// getFullYear() getMonth()->0-11, getDate()->1-31, getHours(), getMinutes(), getSeconds(), getMilliseconds(), getDay()->day of week
// getTime() -> returns timestamp // getTimezoneOffset() -> diff betwen local time zone and utc in minutes
//setFullYear(year [, month, date]), setMonth, setDate, setHours, set Minutes...
let ms = Date.parse("2012-01-26T13:51:50.417-07:00"); // timestamp
let start = Date.now(); // to get the current timestamp fast.
```

#### Destructuring assignment

```js
let arr = ["Ilya", "Kantor"]

// destructuring assignment
let [firstName, surname] = arr; // alert(firstName); // Ilya // if no value, value will be undefined
let [, , title] = ["Julius", "Caesar", "Consul", "of the Roman Republic"]; // alert(title) // Consul

// Works with any iterable on the right-side
let [a, b, c] = "abc"; // ["a", "b", "c"]

let user = {};
[user.name, user.surname] = "Ilya Kantor".split(' ');

// can be used in for of loop
let user = {
  name: "John",
  age: 30
};

// loop over keys-and-values
for (let [key, value] of Object.entries(user)) {
  alert(`${key}:${value}`); // name:John, then age:30
}

// rest operator
let [name1, name2, ...rest] = ["Julius", "Caesar", "Consul", "of the Roman Republic"]; //rest -> ["Consul", "of the Roman Republic"]

// default values: if no value it will be undefined
let [name = "Guest", surname = "Anonymous"] = ["Julius"]; // def values can be assigned instead of undefined // def val can be func etc

// Object destructuring
let {var2, var1} = {var1:…, var2…} // varname order doesnt matter
let {width: w, height: h, title} = options; //var/keyname can be changed
let { width : w = 100, height = 200, title} = options; //default values
let {title, ...rest} = options; // rest is an object
```

### Object

- Property Flags: writable, enumerable(can be looped), configurable

```js
let user = {};

Object.defineProperty(user, "name", { value: "John" }); //defining: for multi defineProperties

let descriptor = Object.getOwnPropertyDescriptor(user, "name"); //getting flags

alert(JSON.stringify(descriptor, null, 2)); // default is true but if set by defineProp it will be all false unless it is assgined //“flags-aware” way of cloning an object: including symbol ones
/* { "value": "John", "writable": false, "enumerable": false, "configurable": false } */ let clone = Object.defineProperties(
  {},
  Object.getOwnPropertyDescriptors(obj)
);

// fully identical shallow clone of obj
let clone = Object.create(
  Object.getPrototypeOf(obj),
  Object.getOwnPropertyDescriptors(obj)
);

Object.preventExtensions(obj); //.seal(obj)//..freeze(obj) //.isExtensible(obj) // .isFrozen(obj)
```

- Getter and Setters

```js
let obj = {
  get propName() {
    // getter, the code executed on getting obj.propName
  },

  set propName(value) {
    // setter, the code executed on setting obj.propName = value
  }
};

// for compability a new prop defined with this
function User(name, birthday) {
  this.name = name;
  this.birthday = birthday;

  // age is calculated from the current date and birthday
  Object.defineProperty(this, "age", {
    get() {
      let todayYear = new Date().getFullYear();
      return todayYear - this.birthday.getFullYear();
    }
  });
}

let john = new User("John", new Date(1992, 6, 1));

alert(john.birthday); // birthday is available
alert(john.age); // ...as well as the age
```

- Unlike primitives, objects are stores and copied by reference.
  A variable stores not the object itself but its adress in memory/reference.
  `let user = { name: "John"}; let admin = user; admin.name = 'Pete'; alert(user.name) // alerts Pete` both points to same reference now

- If two variables points to different objects they are not strictly equal
  `let a = {}; let b= a; alert(a==b)//true alert(a===b);//true`
  `let a = {}; let b= {}; alert(a==b)//false alert(a===b);//false`

- const value can be changed if it is pointing to an object since it is reference based assigment
  `const a = { name: 'John' }; a.name = 'Pete'; //no error, ref is same`

- Object prop can be string or symbol.
- Symbol: unique identifier `let id = Symbol();`
  `let id1 = Symbol("id"); let id2 = Symbol("id"); id1 != id2` ("description")
- Symbols are not auto converted to string `let id = Symbol("id"); alert(id.toString())`
- if key is Symbol there will be no conflict for same values
  `let id = Symbol("id"); myObj[id] = "my value";` if later someone else adds another id value there will no problems.
- Symbol object literal usage: `let myObj = { [symbolVarName]: "val" }`
- Symbols are skipped in for...in loop, Object.assign copies both string and symbol props
- Global symbols: `Symbol.for("desc-name");` read or if not exist creat a symbol registry and acces it: `let id = Symbol.for("id"); let idAgain = Symbol.for("id"); id === idAgain`
- Getting description-name of a symbol: `let sym = Symbol.for("name"); alert(Symbol.keyFor(sym)); //returns name`

- Cloning an object`

`for(let key in obj){clone[key] = user[key]` or
`Object.assign(dest[, src1, src2, src3...]); let clone = Object.assign({}, userObj)` //last prop will be used if same

- If obj has sub objects Object.asign will copy ref again for that prop obj. Deep cloning needed for copying as new

```js
var emptyObj = {}; // or var myObj = new Object();
var myObject = {
    key1: value,
    methodName(){} // shorthand for methodName: function(){}
    "multi word prop": value //myObject["multi word prop"] must be used
};

let fruit = prompt("Which fruit to but?", "apple");
let myBag = { [fruit]: 5 } //computed properties: using square brackets in an object literal

delete myObject.key1; //deleting an object's prop

myObj["name"] = "Charlie"; //or myObj[varName]
myObj.name = "Charlie";

myObj.keyCheck === undefined // if prop not exist it will return undefined, no error thrown // or "nameOfProperty" Obj to check a prop // or varNameOfProp Obj

##### The “for…in” loop (To walk over all keys of an object)
`for(keyAkaProp in objectName){ //executes the body for eacy key in obj }`

function makeUser(name, age) {
  return {
    name, // same as name: name //shorthand
    age: age
    // ...
  };

// obj construction
function Person(name,age) { // constuctor function
  this.name = name; // starts with Uppercase letter
  this.age = age; // must be executed-used with new operator
}

//creates an empty obj, adds props/values, returns this // same as let x = { name: nameval, age: ageval}
var x = new Person(namevalue,agevalue); // if no args var x = new Person // parantheses can be omitted

// adds method to the object other instances can use
className.prototype.newMethod = function() {
statements;
};
#makes a class/obj another class' bitch
Penguin.prototype = new Animal();
```

```js
/* default prototype(prototype is an object with only one prop constructor)
F.prototype = { constructor: F }; //refers to obj itself, if a new obj is created with new keyword, because of this prototype method it will have its constructor set to its creator obj

***we can use constructor property to create a new obj using same constructor as the existing one

function Rabbit(name) { this.name = name; }

let rabbit = new Rabbit("White Rabbit"); // gets a constructor prop equal to Rabbit from prototype parent's prototype = { constructor: Rabbit}
let rabbit2 = new rabbit.constructor("Black Rabbit");*** //rabbit gets parents prototype props as prop

*/

// or F.__proto__ = o; //proto either obj ref or null
```

#### Extras

- Arrow function special: has no this, uses outer scope's this instead , cant be used with new/cant be constructor, has no super

```Js
// Bitwise operators treat args as 32-bit intgeger, if a useage need check MDN's bitwise operators article
// Bitwise operators: AND ( & ) OR ( | ) XOR ( ^ ) NOT ( ~ ) LEFT SHIFT ( << ) RIGHT SHIFT ( >> ) ZERO-FILL RIGHT SHIFT ( >>> )
```

### Patterns

#### Constructor/Prototype Pattern

- methods will be inherited
- With this pattern, you can use the standard #operators and methods on the instances, including #the instanceOf operator, the for-in loop (even hasOwnProperty), and the constructor property.

```js
function User (theName, theEmail) {
    this.name = theName;
    this.email = theEmail;
    this.quizScores = [];
    this.currentScore = 0;
}
​
User.prototype = {
    constructor: User,
    saveScore:function (theScoreToAdd)  {
        this.quizScores.push(theScoreToAdd)
    },
    showNameAndScores:function ()  {
        var scores = this.quizScores.length > 0 ? this.quizScores.join(",") : "No Scores Yet";
        return this.name + " Scores: " + scores;
    },
    changeEmail:function (newEmail)  {
        this.email = newEmail;
        return "New Email Saved: " + this.email;
    }
}
```

#### Parasitic Combination Inheritance Pattern

##### ECMASCRIPT specification .create()

```js
Object.create = function (o) {
   function F() {}

    F.prototype = o;
​
    return new F();
}
```

#example
var cars = { type: "sedan", wheel: 4 }
#new obj toyota will inherit all attr&methods
var toyota = Object.create(cars);

#inherit prototype function for
#creating parasitic combination
#makes the child inherits from paret(super)

```js
function inheritPrototype(childObject, parentObject) {
  var copyOfParent = Object.create(parentObject.prototype);
​
  copyOfParent.constructor = childObject;
​
  childObject.prototype = copyOfParent;
}
```

##### LocalStorage

```js
localStorage.setItem("key", "value");
let lsItem = localStorage.getItem("key")
localStorage.removeItem//sessionStorage also useable
for storage obj lS.("key", JSON.stringfy(lsItem))
JSON.parse(lsItem) // if it is a json obj but stored as string

if(localStorage && localStorage.getItem('thewholefrigginworld')){
  render(JSON.parse(localStorage.getItem('thewholefrigginworld')));
} else {
  $('#list').html(''+loading+'');
  var query = 'select centroid,woeid,name,boundingBox'+
              ' from geo.places.children(0)'+
              ' where parent_woeid=1 and placetype="country"'+
              ' | sort(field="name")';
  var YQL = 'http://query.yahooapis.com/v1/public/yql?q='+
             encodeURIComponent(query)+'&diagnostics=false&format=json';
  $.getJSON(YQL,function(data){
    if(localStorage){
      localStorage.setItem('thewholefrigginworld',JSON.stringify(data));
    }
    render(data);
  });
}
```

#JQUERY

```js
$( document ).ready(function() {
  console.log( 'ready!' );
});

#short version of document ready
$(function() {
  console.log( 'ready!' );
});


if ( $( '#nonexistent' ).length > 0 ) or 1 truty
if ( $("#nonexist").length) //0 is falsy
```

####EVENT DELEGATION####

```js
var list = document.querySelector("ul");
listçaddEventListener("click", function(event) {
  var target = event.target; //bouncing phasage

  while (target.tagName !== "LI") {
    target = target.parentNode;
    if (target === list) return;
  }
  //code
});
```

###EVENT DELEGATION WITH JQUERY

```js
#adds event listener to ul's li s
$('ul').on('click', 'li' function(){});

#adds event handler to click on body
#whenever a button it clickled func plays
$( "body" ).on( "click", "button", function( event ) {
    alert( "Hello." );
});
```

### Async Await, Promises, Callbacks

```js

const posts = [
  { title: "post one", body: "This is post one" },
  { title: "post two", body: "This is post two" },
];

// Callback example
//faking async data
function getPosts(){
  setTimeout( function(){
    let output = "";
    posts.forEach((post, index)=>{ //forEAch iterates through arr, no return
      output += `<li>${post.title}</li>`;
    });
    document.body.innerHTML = output;
  }, 1000);
}

function createPost(post, callback){
  setTimeout(()=>{
    posts.push(post); // push, pop, shift, unshift
    callback(); // it will call cb func after 2s
  }, 2000);
}

createPost({title: "Post Three", body: "This is second post")}, getPosts);

// Promises
function getPosts(){
  setTimeout( function(){
    let output = "";
    posts.forEach((post, index)=>{
      output += `<li>${post.title}</li>`;
    });
    document.body.innerHTML = output;
  }, 1000);
}

function createPost(post){
  //promise takes in a callback(), callback has 2 args: res, rej
  return new Promise((resolve, reject) => {
    setTimeout(()=>{

      posts.push(post);

      const error = false;

      if(!error){
        resolve(); //resolve(123) cant include data-> will be passed to .then function as arg
      } else{
        reject("Error: Something went wrong");
      }
    }, 2000);
  });
}

createPost({title: "Post Three", body: "This is second post")})
.then(getPosts).catch((error) => { console.log(error)});

// Promise.all
const promise1 = Promise.resolve("hello world"); //or new Promise
const promise2 = 10;
const promise3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 2000, "after time we can pass args to callback");
});
const promise4 = fetch("https://jsonplaceholder.typicode.com/users").then((res) => res.json);

// all must resolve or all will be rejected
// Promise.race(): first resolved or rejected promise is returned
Promise.all([promise1, promise2, promise3, promise4]).then((values)=>{
  console.log(values); // an array of resolved values ["hello...", 10, "after.."]
});


// Async / Await
async function init() { // to await must be declared as async
  await createPost({title: "Post Three", body: "This is second post")});

  //it will wait createPosts to be done
  getPosts();
}

init();

// Async-Await Example with Fetch
async function fetchUsers(){
  const res = await fetch("https://jsonplaceholder.typicode.com/users");

  const data = await res.json();

  console.log(data);
}
```

#### Mixin

```js
// mixin
let sayHiMixin = {
  sayHi() {
    alert(`Hello ${this.name}`);
  },
  sayBye() {
    alert(`Bye ${this.name}`);
  }
};

// usage:
class User {
  constructor(name) {
    this.name = name;
  }
}

// copy the methods
Object.assign(User.prototype, sayHiMixin);

// now User can say hi
new User("Dude").sayHi(); // Hello Dude!
```

## Browser: Document, Events, Interfaces

For all nodes: parentNode, childNodes, firstChild, lastChild, previousSibling, nextSibling.
For element nodes only(no text, space etc): parentElement, children, firstElementChild, lastElementChild, previousElementSibling, nextElementSibling.

// returned obj is array like, looping(for, for of: dont use for in) is ok but array methods cant be used(filter...)
// Array.from(document.body.children).filter is possible

Certain elements has additional properties, specific to their type. table:
table.rows, table.caption/tHead/tFoot, table.tBodies
thead, tfoot, tbody has rows property, tr.cells: cells inside tr
tr.rowIndex, td.cellIndex : for getting index of row or cells

document.getElementById("name of id"); // elems with ids directly accessible in js but getElementById is prefered way(name conflicts)
getElementById only usable on document

elem/document.getElementByTagName("tag") // \* for any tags //document.getElementsByTagName("div") gets all divs
elem.getElementsByClassName("class name") / document.getElementsByName("only can be used on document.gEsBN")

elem.querySelectorAll("css selector") // document.querySelectorAll("ul > li:last-child)

elem.querySelector(css) //returns first element for given css selector

elem.matches("css selector") // returns true or false

elem.closest("css selector"): looks the nearest ancestor(parents) that matches css selector // elem will not be match if it is a sibling/not ancestor/parent // elem itself is in search
elem.contains(elem2) // returns true if element is inside or equal

getElementsBy\*\*\*(except Id) : creates a live collection, always reflects current state of document and auto updated when it changes
querySelectorAll returns a static collection

document.body.innerHTML: to change an elements html(inside), leads to reinstalling assets etc. if script inserted it will threated as already run
document.body.outerHtml: to replaces/gets html, including element. changed element will be removed/replaced from dom but will still exist in selection

nodes(text, comment etc) uses data-> commentNode.data (or .nodeValue): returns text

textContent: used to acces text inside the element, only text minus all tags

elem.hidden = true === elem.style.display = "none" // adds a hidden attr to element, shorter version

elem.value: value of input, select, textarea
elem.type: getting type of input etc
elem.href: href of link
elem.idName: to get an element with id idName

elem.nodeType: 1 for elements, 3 for text elements

element properties like id etc can be accessible elem.id
custom props can be assigned like document.body.myData = {...} or
Element.prototype.sayHi = function(){...} //also possible

elem.hasAttribute("attr name") // returns true or false
elem.getAttribute("attr name") // returns a string, if boolen than empty string, string will be exactly like html
elem.setAttribute("name", "value")
elem.removeAttribute("attr name")
elem.attributes // collection of all attributes

data-\* attributes are accesible with dataset
elem.dataset.nameOfDataAttrIfMultiThenCamelCaseForJsCssIsSame

Methods to create new nodes:

document.createElement(tag) – creates an element with the given tag(createTextNode creates text node, rarely used)
elem.cloneNode(deep) – clones the element, if deep==true then with all descendants.
Insertion and removal of nodes:

From the parent:
parent.appendChild(node)
parent.insertBefore(node, nextSibling)
parent.removeChild(node)
parent.replaceChild(newElem, node)
All these methods return node.

New Methods for inserting...
node.append(...nodes or strings) – insert into node, at the end,
node.prepend(...nodes or strings) – insert into node, at the beginning,
node.before(...nodes or strings) –- insert right before node,
node.after(...nodes or strings) –- insert right after node,
node.replaceWith(...nodes or strings) –- replace node.
node.remove() –- remove the node.
Text strings are inserted “as text” lite textContent. Multi insert possible

Given a piece of HTML: elem.insertAdjacentHTML(where, html), inserts depending on where:

"beforebegin" – insert html right before elem,
"afterbegin" – insert html into elem, at the beginning,
"beforeend" – insert html into elem, at the end,
"afterend" – insert html right after elem.

Also there are similar methods elem.insertAdjacentText and elem.insertAdjacentElement, they insert text strings and elements, but they are rarely used.

To append HTML to the page before it has finished loading:

document.write(html)
After the page is loaded such a call erases the document. Mostly seen in old scripts.

className – the string value, good to manage the whole set of classes.
classList – the object with methods add/remove/toggle/contains, good for individual classes.

style.cssText //whole css attributes
getComputedStyle(elem[, pseudo]) //after css applied by browser

offsetParent(nearest positioned or td, th, table, body)
offsetLeft/offsetTop(distance to offsetParent-after border)
offsetWidth/offsetHeight – outer width: including borders
clientLeft/clientTop – border + scroll
clientWidth/clientHeight – content width/height - scroll
scrollWidth/scrollHeight – the width/height of the content including the scrolled out parts. Also includes paddings, but not the scrollbar.
scrollLeft/scrollTop – how much scroolled from top/left(if scroll)

All properties are read-only except scrollLeft/scrollTop. They make the browser scroll the element if changed.

##### Window sizes and scrolling

```js
document.documentElement; // <html>

window.innerWidth; // full window width
document.documentElement.clientWidth; // window width minus the scrollbar

//freezing scrool
document.body.style.overflow = "hidden"; // locks scrool at current pose
document.body.style.overflow = ""; // unlocks scrooling

// Summary

// visible part of document(content area width/height)
document.documentElement.clientWidth / Height;

// Width/height of the whole document, with the scrolled out part:
let scrollHeight = Math.max(
  document.body.scrollHeight,
  document.documentElement.scrollHeight,
  document.body.offsetHeight,
  document.documentElement.offsetHeight,
  document.body.clientHeight,
  document.documentElement.clientHeight
);

// Read the current scroll:
window.pageYOffset /
  pageXOffset.window // Change the current scroll:
    .scrollTo(pageX, pageY); // absolute coordinates,
window.scrollBy(x, y); // scroll relative the current place,
elem.scrollIntoView(top); // scroll to make elem visible (align with the top/bottom of the window).
```

#### Coordinates

```js
elem.getBoundingClientRect(); //returns window coordinates for elem as an object with properties(from 0,0 coords)
document.elementFromPoint(x, y); // to get most nested elem at point

elem.getBoundingClientRect; // gets elems position values(top, left, bottom, right like css position values)
//creates a message under an element, then it is removed, will be scrooled since it is position fixed
let message = createMessageUnder(elem, "Hello, world!");
document.body.append(message);
setTimeout(() => message.remove(), 5000);

// Document coordinates
clientX, clientY; // relative to window/viewport
pageX, pageY; // stays same when scrolled

// get document coordinates of the element
function getCoords(elem) {
  let box = elem.getBoundingClientRect();

  return {
    top: box.top + pageYOffset,
    left: box.left + pageXOffset
  };
}
```

### Browser Events

```js
/* Mouse events:

click – when the mouse clicks on an element (touchscreen devices generate it on a tap).
contextmenu – when the mouse right-clicks on an element.
mouseover / mouseout – when the mouse cursor comes over / leaves an element.
mousedown / mouseup – when the mouse button is pressed / released over an element.
mousemove – when the mouse is moved. */

/* Form element events:

submit – when the visitor submits a <form>.
focus – when the visitor focuses on an element, e.g. on an <input>. */

/* Keyboard events:

keydown and keyup – when the visitor presses and then releases the button.
Document events:

DOMContentLoaded – when the HTML is loaded, DOM is fully built.

CSS events:
transitionend – when a CSS-animation finishes. */

/*** EVENT HANDLERS  ****/

// Event delegation: instead of adding listener to every elem, adding it to container and getting event.target

<input ... onclick="alert('Click!')" /> // html attribute
<input ... onclick="countRabbits()" .../> // html attr, func

// event handler as dom property // only one handler can be assigned
elem.onclick = function(event) { // or elem.onclick = someFunc;
  alert('Thank you');
};

elem.onclick = null; // removing handler

// this, inside a handler is that element

// add/removeEventListener
element.addEventListener(event, handler[, options]); //if multiple handler for same action all will be called
//options: an object {once, capture}

element.removeEventListener(event, handler[, options]);)

// browser passes down an event object to handler function
e.currentTarget, e.target, e.type

// if handler is an object, object.handleEvent(e) will be called
// let a new instanceOfAClassEtc // elem.addEL("click", instanceOfAClassEtc) // instanceOfAClassEtc.handleEvent(e) will be called

// When an event happens on an element, it first runs the handlers on it, then on its parent, then all the way up on other ancestors.
// Almost all events bubble ( not focus etc)
// event.target: where event happened // e.currentTarget: current target/this

event.stopPropagation() // stops event bubbling // stops upwards bubbling
event.stopImmediatePropagation() // no other handler will be invoked

event.preventDefault() // stops default behaviour of elem
<a... onclick="return false">...</a> // or if on<Event> assigned, return false(not addEventlistener)
```
