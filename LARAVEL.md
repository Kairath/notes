- composer global require "laravel/installer"
- laravel new blogEtc
- php arsitan serve // artisan: laravel's cmd utility

##### mysql settings // change .env file: DB_NAME, DB_PASSWORD
```
mysql -uroot -p
---in mysql shell
create database blog;
show tables;
```


##### routes/web
```
Route::get("/", function(){
   return view("welcome"); // welcome.blade.php 
});
```

##### web.php
```
Route::get("/", function(){
   return view("welcome", [
    "name" => "var name sent to view with this value",
    "anotherPassedvar" => "value"
   ]);
   
   -- or 
   // sends view $name with value of asd
   return view("welcome")->with("name", "asd");
    
   -- or 
   // sends view $name with value of asd
   return view("welcome", ["name"=>"value"]);
   
   -- or 
   // sends view $name with value of asd
   return view("welcome", compact("name", "anotherVarname");
});
```

- php artisan migrate // migrate refresh for recreating all migs


```
@if(cond) // @ shorthand for <?php ?>
    <li>{{$asd}}</li> // {{}} means echo whats inside
@endif

```


- sublime text: ctrl + r -> rweb (fuzzy matching: routes/web)

- php artisan make // file generations
- php artisan make:migration create_task_tables --create=tasks //creates migra file for tasks table
- composer dump-autoload // for dumping autoload files if there is a file change/file error

```
$tasks = DB::table("tasks")->get(); // gets all records from task table
//if route returns tasks-> view will shows json
// or return view("welcome", compact("tasks");
```

```
Route::get("/tasks/{id}", function($id){ // wildcard
    dd($id); // dump and die, other code not gonna work
    $task = DB::table("tasks")->find($id);
    return view(...);
}
```

```
DB::statement("drop table user"); // general statement
$tasks = DB::select("SELECT * FROM tasks WHERE id=?", array($id);
$updateEx = DB::update("UPDATE t_name SET votes=?, asd=?", array(10, "lalala");

```

- php artisan make:model Task -cm // cm means with controller and migration

```
App\Task::all(); // fetchs all tasks
App\Task::where("id", ">", 2)-get();
App\Task::pluck("body", "title"); // just body and title vals returned
```

- use App\Task; // we can reference it now as Task

```
$task = Task::find($id);
```

- php artisan help

```
php artisan help make:model // gives info for make:model
```

- php artisan tinker

```
$newTask = new App\Task;
$task->body = "asd";
$task->save();
```

```
public static func foo(){
    return static::where("completed", 0)->get(); // static equals to $this or Task::
}

// if scopeFunc used laravel knows there will be a query, after $query, other values can be passed
//will be run if ::incomplete used, laravel knows there will be a query
//used for chaining query
//returns instance of Eloquent query builder
public function scopeIncomplete($query, $othervar){
    return $query->where("completed", 0);
}

// in tinker
App\Task::incomplete()->get(); // scopeIncomplete runs before get(), magic
```

##### Controller

```
//web
Route::get("task", "TaskController@index"); // if there is get req for task page use index method

//TaskController
public func index() {}
public function show($id) {
    $task = Task::find($id);
    return view("task.show", compact($task); // or task/show
}

// or

// means Task::find($task)
public function show(Task task) { // willdcard must be {task}
    return $task;
}

```

##### Layout

```
--- layout.blade.php
    ...
    @yield("content");
    ...
    
---- in view file for using layout
    @extends("layout");
    @include("partial_nav"); // using a partial
    @section("content") // this will be used in layout yield("content");
        #code 
    @endsection


```

##### LARAVEL RESTFUL

```
Verb	    Path	                Action	        Route Name

GET	        /photo	                index           photo.index
GET	        /photo/create	        create	        photo.create
POST	    /photo	                store	        photo.store
GET	        /photo/{id}	            show	        photo.show
GET	        /photo/{id}/edit	    edit	        photo.edit
PUT/PATCH   /photo/{id}	            update	        photo.update
DELETE	    /photo/{id}	            destroy	        photo.destroy
```


##### FORM VALIDATION
- use html required attr and type fields for html/client side validation
- in Controller: server side validations
```
$this->validate(
    request(), [
        "title" => "required",
        "email" => "required|email"
    ]
)
//if validate fails auto redirects previous page with $errors
//in form
@if(count($errors)
    <ul>
    @foreach($errors as $error)
        <li>{{$error}}</li>
    @endforeach
    </ul>
@endif
//if validate fails -> for old vals {{ old('title') }} 
// request::old("body") -> in Ctrls, Model // check this feature
```

##### carbon lib
```
$post->created_at->toFormattedDateString();
            //or ->diffForHumans() // ... ago
```

##### webpack: scss, es6 transformations
- needs nodejs, npm
```
npm install
npm run dev // or npm run watch
```

- laravel uses axios: an ajax library

##### hasMany relation

```
php artisan make:model Comment -m
class Post extends Model {
    public func comments(){
        return $this->hasMany(Comment::class); //Comment::class - class path- App\Comment
    }
    
    $post= App\Post::find($id);
    $post->comments; // will be called as prop, laravel will eager load relationship
}

---

class Comment extends Model {
    public function post(){ // naming: we wanna call it as comment->post
        return $this->belongsTo(Post::class);
    }
}

---
// now in view we can say
@foreach($post->comments as $comment)
```

- form needs hidden {{method_field("PATCH")}} for patch reqs via Post
- form needs {{ csrf_field() }} field for csrf error


```
Route::post("/posts/{post}/comments", "commentsController@store");
---commentCtrl.php
public func store(Post post){
    Comment::create([
        "body" => request("body"),
        "post_id" => $post->id
    ]);
    
    return back(); // sugar for redirecting prev page
}
```

##### AUTH

```
php artisan make:auth /// boilerplate for auth stuff

---HomeController.php
$this->midleware(["auth", ["only" => "index"]) // or except
```

```
// bcrypt pass, validate, create user, login
bcrypt("pass"); 
$this->validate(request(), [
    "name" => "required", 
    "email" => "required|email",
    "pass" => "required|confirm"
    ]);
    
$user = User::create(request(["name", "email", "password"]);

auth->login($user); // for logout auth->logout();
if(auth::check()) // auth::chech() returns true if there is signed user
auth()->user()-id // or auth()->id();
$this->posts()->save($post); // $this refers user, create() needs field, save passes model

---session controller
public function store(){
    if(auth()->attempt(request(["email", "password"]))){ // attempt() tries to login
        return back(); // return back->withErrors(["message" => "missing bla bla bla"])
    } else {
        redirect()->home(); // Route must be like Route...->name("home");
    }
}

public function __construct(){
    $this->middleware("guest"); // only guests can read Session ctrl, except => destroy can be used
    
}
```


```
// archives
//sql
SELECT year(created_at) as year, montname(created_at) as month,
        count(*) as published
FROM posts
GROUP BY year, month
ORDER BY min(created_at) DESC

//
App\Post::selectRaw("year(created_at) year", ...)
            ->groupBy("year", "month")->get()->toArray();
            
            
if(month == request("month"){
    $posts = where(month("created_at", Carbon::parse($month)->month); // carbon gets m as number
}  
    
// post.php ???
public static func archive(){
    return static::selectRaw("year(created_at", ...)->groupBy...;
}

```

##### Service Provider // 2 methods: register and boot

```
// when layouts.sidebar view called this will run ???
public func boot(){
    \View::composer("layouts.sidebar", function($view){ // or instead $view, class path
        $view->with("archives", App\Post::archives();
    }
}

```

##### PHPUNIT // for browser testing: check laravel dusk

```
// phpunit fileName -> if not working: cmd -> alial phpunit = "vendor/bin/phpunit"
// testing: given(what we have), when(x happens), then(must be like)

$response = $this->get("/");
$response->assertStatus(200);
$response->assertSee("login");

```

- model factories like a blueprint for Eloquent model
```
Factory("App\User", 20)->make(); // not persistent, creates 20 user

---db/factories

factory->define(App\Post::class, function(Faker\Generator $faker){
    return [
        "title" => $faker->sentence,
        "body" => $faker->paragraph,
        "user_id" => factory("App\User")->create()->id;
    ]
}

// add a line for override database name(user_test etc)
// change env file to _test, migrate, then change it back

--in test file
use DatabaseTransactions; //after test class def, will delete records after test finished
//given
$first = factory(Post::class)->create();
$second = factory(Post::class)->create(["created_at" => \Carbon\Carbon::now()->subMonth()]);

//when
$post= Post::archives();
//then
$this-> assertCount(2, $posts);

//env file DB_DATABASE value="blog_testing" // changed
// use DatabaseTransactions; // rolls back when test finished, added after test class defination

$this->assertEquals(["year" => $first->created_at->format("Y"), month, published], $posts);

```

```
//app-repositories/postRepository
namespace App\Repository;

class Posts{
    public function all(){
        return Post::all();   
    }
}

/postCtrl.php
use App\Repository\Posts;

public func index(Posts $posts){
    $posts = $posts->all();
}

//laravel creates App\Repo\Posts instance using Dependency Injection
// DepInj: means passing args to funcs
//laravel auto creates insances when classes passed as arg
```

```
//registering a class to ServiceContainer // left name of the stuff, right arg is the value
App::bind("App\Billing\Stripe", function(){
    //passes config file val as arg // second arg must be a class or function
   return new \App\Billing\Stripe(config("services.stripe.secret")); 
});

//creating a class instance // calling service provider
App::make("App\Billing\Stripe"); // there is also a resolve(alias of make), and singleton method

---App\Billing\Stripe.php
namespace App\Billing;
Stripe {
    pub func __construct($key){
        $this->key = $key;
    }
}
```

```
//modelfactory.php
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

//PostTableSeeder
public function run()
{
    factory(App\Post::class, 30)->create();
}
    
//DatabaseSeeder

public function run()
{
    $this->call(UserTableSeeder::class);
	$this->call(PostTableSeeder::class);
	$this->call(CommentTableSeeder::class);

	// factory(App\User::class, 10)->create()->each(function ($u) {
    //     $u->posts()->save(factory(App\Post::class)->make());
    // });


}
```

```
// AppServiceProvider
protected $defer = true; // if there isnt boot stuff, for only loading once
public function boot()
    {
        // when layouts.sidebar used, bind view a archives variable
        view()->composer("layouts.sidebar", function($view){
            $view->with("archives", \App\Post::archives()); // varName, value
        });
    }
    
public function register() // for registering things to service container
    {
        \App::bind("key", function(){ // or $this->app->bind/ or singleton() etc
           return #code 
        });
    }
    
    
```

    
```
// php artisan make:request RegistrationRequest
// rule method->validates form
//authorize/ if ebody can acces, then it should return true
// add to registration ctrl method as arg: RegistrationRequest $request

// request([...]) equalst to request()->only([]); // so in request class -> $this->only([...])
    
```


```
//session // session()->flash(); redirect -> will be avail in redirect controller method till refresh
session("msg", "gimmi msg or this default val will be returned");
session(["key" => "if itz an array, then it will set"]);

session()->flash("msg", "default"); // only for 1 page load, after redirect will be no longer available
```

```
//ManyToMany Relationship
// posts, tags tables -> p comes first + singular -> post_tag table 
// post_tag migration
$table->ingeter("post_id");
$table->ingeter("tag_id");
$table->primary(["post_id", "tag_id"]);

//Post model
pub function tags(){
    return $this->belongsToMany(\App\Tag::class); // same for tags
}

// for n+1 problem
App\Post::with("tags")->get(); // laravel eager loads, posts comes with tags

//
$post = App\Post::first();
$tag = App\Tag::where("name", "personal")->first();

$post->tags->attach($tag); // creating relation, $tag ro $tag->id // there is also detach method
```