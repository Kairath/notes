- PHP allows single inheritance ( max 1 super)
- inherited class have all meths, props except private ones
- for accessing super's methods -> super::method(), super::prop
- for accesing self methods, props -> self::method(), self::prop
```
class myClass extends OtherClass
{
    public $var1 = "public var can be accessed anywhere";
    protevted $var2 = "can be accessed by the class and subclasses";
    $private $var3 = "can be accessed by the class only, not even subclasses can asses it";
    
    //called when instance created
    public function __construct() { 
        $this->prop = "asd";
    } 
    
    //called when instance destroyed(from memory or garbage collector)
    public function __destruct() {} 
    
    
    constant A = 2;
    
    function printHello { #code }
    static function aStaticMethod { #code }
}

$obj = new myClass();
$obj->printHello();
echo $obj->var1 // var2, var3 = fatal error

```

- :: allows to access constants and static methods
- static : we dont need a class instance for accessing
- static : by default have public accesibility, $this is not available
```

echo $obj::A;
$obj::aStaticMethod();
```

- abstract class, used for must overriden method for sub classes or 
- or must be abstract themselves
```
abstract class myAbClass
{
 public punc d();   
}

class B extends myAbClass{
    protected func A(){#code} // must be overriden
}

```

##### interfaces
```
interface myInterface
{
    public function aMethod($arg);
}

class Template implements myInterface
{
    public function aMethod($arg){
        #code
    }
}

```

##### traits
```
trait myTrait {}

class myC {
    use myTrait, secondTrait;
}

---

// for name collision error
class mAnotherC{
    use myT1, myT2{
        myT2::aMethod instead of MyT2;
        myT1::aMethod as renamingCollapsedMethod();
    }
}

$a = new mAnotherC();
$a->aMethod(); // calls myT2::aMethod
$a->renamingCollapsedMethod() // calls myT11::aMethod

---

// for name collision error
class mAnotherC{
    use myT1, myT2{
        myT2::aMethod instead of MyT2;
        myT1::aMethod as renamingCollapsedMethod();
    }
}

---

class TraitWithUseAsExampleClass{
    use myTrait {
        //also can change visibility with name
        aMethod as private aPrivateMethod; 
    }
}

```
- traits can use other traits
- final method cant be overriden


##### Object Equality

```
$a = new MyClass();
$b = new MyClass();
$c = $a; // by reference

$a == $b;  // true
$a === $b //false
$a == $c  // true
$a === $c // true

```

- all visible propss can be itareted using foreach loop

##### Auto Loading Classes

```
spl_autoload_register(function($className){
    include $className.".php";
});

//spl_autoload_register func
//auto loads classes and interfaces
//if not defined before it fails with an error
a = new A(); 

```

##### Magic Methods ??? // read again

```
__construct(), __destruct(), __toString(), __set(), 
__isset(), __unset(), __call(), __callstatic();
__invoke, __set_state, __clone, __sleep,
__wakeup and __debugInfo

```