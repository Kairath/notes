# React

```js
ReactDOM.render(JSX, document.getElementById('root'));

// html changes: className(instead of class), other things are camelCase: onClick, htmlFor for labels etc
{/* */} //between {} will be treated as javascript

// Functional component. Funcname must start with uppercase(lovewcase treated as html tag) and func should return jsx or null
const Menu = (props) => { or  function Menu({dish, onClick}){
  return (...jsx stuff...)
}

//Class Component

class DisplayMessages extends React.Component {
  constructor(props) {
    super(props);
    this.state = { //initial state
      input: 'initial',
      messages: ["a", 3]
    }
  }

  // changing state in a func with .setState(): accepts an object with key value pairs
  handleClick() { // or handleClick = () => {...} to not to bind this
    this.setState({input: "settingNewValtoState"})
  }

  render() { // must return a component or null, html elements and components need closing tag -> <br> </br>
    const input = this.state.input; // after render and before return u can do operations by directly using js on state

    return (
    	<App>
        <RenderingComponent propName="value"/> //or use {} for values, will be treated as js
        <div>{this.props.propName}</div> // this keyword used for accesing props despite stateles comps
        <button onClick={this.handleClick}>Click Me</button>
      </App>
    );
  }
};

ComponentName.defaultProps = { location: 'San Francisco' }
MyComponent.propTypes = { handleClick: PropTypes.func.isRequired }; //import PropTypes from "prop-types"
```

### Binding this to Handler Function

```js
constructor(props) {
    super(props);
    this.addItem = this.addItem.bind(this); // or bind in func(creates a new func every time): onClick = {addItem.bind(this)}
  }

  // or addItem = () => {...} for not binding it in constructor
  addItem() { /* some code that uses this */ }
```

### onChange event passes an event var to function

- `<input value={this.state.input} onChange = { this.handleOnChangeEventFunction } />`
- form has onSubmit handler, input has onChange
- event.preventDefault() for preventing form submit events etc
- textarea is self closing in React and accepts a value attr
- select has a value attr in react, `<select multiple={true} value={["a", "b"]}>` for multiple selection
- file input is uncontrolled component in React `<input type="file" />`
- event.type == 'checkbox' to check if it is a checkbox, event.target.checked for getting checkbox value
- `let name = e.target.name` for getting name attr of input, then `this.setState({[name]: event.target.value}`
- specifyn value prop on controlled comp prevents user input (till prop is set to null)

### Lifecycle methods

Mounting: constructor(), static getDerivedStateFromProps(), render(), componentDidMount()
Updating: static getDerivedStateFromProps(), shouldComponentUpdate(), render(), getSnapshotBeforeUpdate(), componentDidUpdate()
Unmounting: componentWillUnmount()
Errors: static getDerivedStateFromError(), componentDidCatch()

```js
 componentDidUpdate(prevProps, prevState, snapshot){}
 shouldComponentUpdate(nextProps, nextState)
 static getDerivedStateFromProps(props, state) //runs before render methods both initial and rerenders
 //getDerivedStateFromProps returns an object to update state or null to update nothing
 //used when state should be updated when props change
 getSnapshotBeforeUpdate(prevProps, prevState) // returned value will be passed to CDUpdate
 static getDerivedStateFromError(error) // update state here so next render will whow error message etc
 componentDidCatch(error, info) // log to external service etc
```

### React inline css

```js
// instead of <div style="color: yellow; font-size: 16px">Mellow Yellow</div>
// since react uses compiled js it should be camel cased(font-size = fontSize) and as a js object
<div style={{ color: "yellow", fontSize: 16 }}>Mellow Yellow</div> //fontSize 16: px part can be ommited // px default
```

### Rendering an array

```js
// inside render function
const items = this.state.toDoList.map(i => <li key={i}>{i}</li>); // mapped jsx needs key attr to keep track of changed ones
return (... <ul>{items}</ul> //array of elements can be used ...)
```

### Rendering React on Server

```js
ReactDOMServer.renderToString(<App />); // renders react on server
```

#### React.Fragment: for grouping react comps

`return ( <> ...comps... </>)` or `return ( <React.Fragment> ...comps... </React.Fragment>)` if key used

# Redux

```js
const reducer = (state = 5, action) => { // reducer: pure func which returns new state
  return state; // only job: returns new state, no side effects or api calls
}

const reducers = combineReducers({reducer1, reducer2: reducer2});

//accepts reducer(s) as arg, getStore has a prop for each reducer's state
let store = Redux.createStore(reducer);  //2nd arg initialState, if combineReducer used must be an object

let initial-data-from-local-storage = (){
  try{
    let stateData = localStorage.getItem("state");
    if(!stateData) return undefined;
    return JSON.parse(stateDate);
  } catch(e) { console.log(e); return undefined;}
}
let store = Redux.createStore(reducer, initial-data-from-local-storage);

store.subscribe(saveStateToLocalStorage( ()=> {store.getState()} ));

//store has 3 important methods
store.getState(); // getting current state
store.dispatch({type: "INCREMENT"}); // dispatching an action // action: js object with type and optional payload properties
store.subscribe(cb) // registering a cb, store will call when an action dispacted
```

- to dispatch an action you can create a bound func: `const boundAddTodo = text => dispatch(addTodo(text))`
- action creator: js function which returns an action(js object) `const loginAction = () => {type: 'LOGIN'};`
- store.dispatch(loginAction()): used to dispatch an action to redux store.
- common practice is asigning type values/strings as constants(read-only vars): const LOGIN = "LOGIN";

### Redux Async Calls

- For async calls ReduxThunk(or saga) middleware is used
- Redux middleware args: store, nextDispatch, action
  const thunk = (store) => (nextDispatch) => (action) => typeof action === "function" ? action(store.dispatch) : next(dispatch)

`const asynActionCreator = (argsWhenItsCalledOrEmpty) => (dispatch, getStore) => {}`
`const handleAsync = () => (dispatch) =>{ /*dispatch etc then return a promise to keep chaining */}`
`applyMiddleware(thunk.withExtraArgument(argNameLikeApi etc))` `function fetchUser = () => (dispatch, getStore, extraArg) =>{}`

- Middleware called with every action dispatch

- Redux thunk allows to return a function and if func returned
  thunk calls that function, passes dispatch and getState(store) as arguments

```js
import { connect } from 'react-redux'
// to wrap connect with it, to get router related props
import {withRouter} from "react-router";

//how props should be // returns a js object with prop key-val pairs
const mapStateToProps = state => { // component props can be passed as an arg here (state, props/or ownProps)
  return {
    todos: state.todos
  }
}

// how dispatch should be passed. returns a func which dispatches action
const mapDispatchToProps = dispatch => {
  return {
    onTodoClick: id => {
      dispatch(toggleTodo(id))
    }
  }
}
​
// if both mstp, mdtProps passed null, default behaviour is not subscribe to store and just pass dispatch as prop
const VisibleTodoList = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps // or an object for methods { onTodoClick: toggleTodo} //methodName: action creator
  //if MapDisToP is obj with actionCreator values, dispatched auto
)(TodoList));

//todolist now accepts dispatch as arg
let TodoList = ({ dispatch }) => { return ...jsx... }
​
export default VisibleTodoList
```

### to pass store to all components: use Provider -> need to specify which props and state component needs

with mapStateToProps() and mapDispatchToProps() functions

```js
import { Provider } from 'react-redux'

const store = createStore(todoApp)
​
render(
  <Provider store={store}>
    //<Router etc
    <App />
  </Provider>,
  document.getElementById('root')
```

### React Additional Notes

`import("./math").then(math => { console.log(math.add(16, 26)); });` dynamic import

```js
//lazy only supports default exports and accepts a func that returns a promise
const OtherComponent = React.lazy(() => import("./OtherComponent"));

function MyComponent() {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <OtherComponent />
      </Suspense>
    </div>
  );
}
```

- React Ref: used for uncontrolled component etc
- Can be wrapped in ErrorBoundary Comps to catch loading errors

```js
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
  }

  focus() {
    this.textInput.current.focus();
  }

  render() {
    return ( <input type="text" ref={this.textInput}
      />
    );
  }
}


// using ref for file input
constructor(props) {
    this.fileInput = React.createRef();
  }
  handleSubmit(event) {
    // file api used for interacting with files
    alert(
      `Selected file - ${
        this.fileInput.current.files[0].name
      }`
    );
  }

  render() {
    return (...
      Upload file:
      <input type="file" ref={this.fileInput} />
    ...);
  }

// forwarding ref
//second arg ref is there because of Reacd.forwardRef()
const FancyButton = React.forwardRef((props, ref) => (
  <button ref={ref} className="FancyButton">
    {props.children}
  </button>
));

// insted of this.input = createRef(), a const used for holding ref
const ref = React.createRef();
<FancyButton ref={ref}>Click me!</FancyButton>;

// HOC forwarding ref

function logProps(Component) {
  class LogProps extends React.Component {
    componentDidUpdate(prevProps) { /*...*/}

    render() {
      const {forwardedRef, ...rest} = this.props;

      return <Component ref={forwardedRef} {...rest} />;
    }
  }

  //if hoc used this will be returned component
  //which uses forwardRef to acces its ref and passes down to wrapped comp as forwardedRef
  return React.forwardRef((props, ref) => {
    return <LogProps {...props} forwardedRef={ref} />;
  });
}
```

- Adding/removing event listener in cdm and cwu

#### Context

```js
// Create a context with default value 'light'
const ThemeContext = React.createContext("light");

class App extends React.Component {
  render() {
    return (
      <ThemeContext.Provider value="dark">
        {" "}
        /* passing context to down with value set to dark */
        <Toolbar />
      </ThemeContext.Provider>
    );
  }
}

class ThemedButton extends React.Component {
  // Assign a contextType to read the current theme context.
  // React will find the closest theme Provider above and use its value.
  // In this example, the current theme is "dark".
  static contextType = ThemeContext; // or use Context Consumer
  render() {
    return <Button theme={this.context} />;
  }
}
// or here ThemedButton.contextType = ThemeContext;
```

- Multi Context

```js
// A component may consume multiple contexts
function Content() {
  return (
    <ThemeContext.Consumer>
      {theme => (
        <UserContext.Consumer>
          {user => <ProfilePage user={user} theme={theme} />}
        </UserContext.Consumer>
      )}
    </ThemeContext.Consumer>
  );
}
```

#### ErrorBoundaries

- Cant catch errror for: event handlers(click etc), async code(setTimeout, requestAnimationFrame callbacks etc), server side rendering, errrors thrown in the boundrarys itself(rather than children)

```js
class ErrorBoundary extends React.Component {
  this.state = { hasError: false };


  // according to docs this one should be used for updating ui
  // @returns new state
  //called during render, so no side effects permitted
  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  // docs: it can be used for updating ai with setState but will be deprecated in future so use gDSFE for ui update
  // used for loging errors to error reporting services etc
  componentDidCatch(error, info) {
    logErrorToMyService(error, info);
  }

  render() {
    if (this.state.hasError) {
      return <h1> Something went wrong </h1>;
    }
    return this.props.children;
  }
}

// then use it as regular component and wrap other components to catch error
<ErrorBoundary>
  <ThisCompWrappedByErrorBoundary />
</ErrorBoundary>;
```

#### Props.Children

```js
function FancyBorder(props) {
  return (
    <div className={"FancyBorder FancyBorder-" + props.color}>
      {props.children} // refers to nested component when used
    </div>
  );
}

function WelcomeDialog() {
  return (
    <FancyBorder color="blue">
      {" "}
      // nested comps will be displayed in props.children
      <h1 className="Dialog-title">Welcome</h1>
      <p className="Dialog-message">Thank you for visiting our spacecraft!</p>
    </FancyBorder>
  );
}
```

- or props can be used to store elements and pass to a component

```js
function SplitPane(props) {
  return (
    <div className="SplitPane">
      <div className="SplitPane-left">{props.left}</div>
      <div className="SplitPane-right">{props.right}</div>
    </div>
  );
}

function App() {
  return <SplitPane left={<Contacts />} right={<Chat />} />;
}
```

#### Higher-Order Components (HOCs): A comp that takes a component as arg and returns a new component

```js
//display name of hoc
function withSubscription(WrappedComponent) {
  class WithSubscription extends React.Component {
    /* ... */
  }
  WithSubscription.displayName = `WithSubscription(${getDisplayName(
    WrappedComponent
  )})`;
  return WithSubscription;
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || "Component";
}
//display name of hoc

const CommentListWithSubscription = withSubscription(CommentList, DataSource =>
  DataSource.getComments()
);

function withSubscription(WrappedComponent, selectData) {
  // ...and returns another component...
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
      this.state = {
        data: selectData(DataSource, props)
      };
    }

    componentDidMount() {
      // ... that takes care of the subscription...
      DataSource.addChangeListener(this.handleChange);
    }

    componentWillUnmount() {
      DataSource.removeChangeListener(this.handleChange);
    }

    handleChange() {
      this.setState({
        //or this.setState((state, props) => { return {counter: state.counter + props.step}; } // guaranteed up to date props
        // this.setState((state, props)=> {}, callBackFuncWhichWillBeCalledAfterSetState) // or setState({}, ()=>{})
        data: selectData(DataSource, this.props)
      });
    }

    render() {
      // ... and renders the wrapped component with the fresh data!
      // Notice that we pass through any additional props
      return <WrappedComponent data={this.state.data} {...this.props} />;
    }

    // or with filtering and adding extra props
    render() {
      // Filter out extra props that are specific to this HOC and shouldn't be
      // passed through
      const { extraProp, ...passThroughProps } = this.props;

      // Inject props into the wrapped component. These are usually state values or
      // instance methods.
      const injectedProp = someStateOrInstanceMethod;

      // Pass props to wrapped component
      return (
        <WrappedComponent injectedProp={injectedProp} {...passThroughProps} />
      );
    }
  };
}

//static methods must be coppied over
function enhance(WrappedComponent) {
  class Enhance extends React.Component {
    /*...*/
  }
  // Must know exactly which method(s) to copy :(
  Enhance.staticMethod = WrappedComponent.staticMethod;
  return Enhance;
}

//or hoist non react static can be used to copy
import hoistNonReactStatic from "hoist-non-react-statics"; //a lib
function enhance(WrappedComponent) {
  class Enhance extends React.Component {
    /*...*/
  }
  hoistNonReactStatic(Enhance, WrappedComponent);
  return Enhance;
}

//Another possible solution is to export the static method separately from the component itself.
// Instead of...
MyComponent.someFunction = someFunction;
export default MyComponent;

// ...export the method separately...
export { someFunction };

// ...and in the consuming module, import both
import MyComponent, { someFunction } from "./MyComponent.js";
```

#### Portals

An event fired from inside a portal will propagate to ancestors in the containing React tree, even if those elements are not ancestors in the DOM tree.
`render(){ return (ReactDOM.createPortal(childLikeThisPropsChildren, containerHtmlNode))}`

#### Strict Mode: Adds additional checks for wrapped components

```js
render(){
  return(
    <React.StrictMode>
      <ChildComponents />
    </React.StrictMode>
  )
}
```

### GATSBY

npm i -g gatsby-cli
gatsby new <projectName> (cd there, code . to open files in editor)
gatsby develop (in project folder: fires dev server->localhost:8000)
gatby build

### REACT TESTING

Unit Tests: Test one piece (usually one function)
Integration Tests: How multiple units work together
Acceptence/End-toEnd(E2E) Tests: How a user would interact with app(with headless browser)

Enzmye: Creates a virtual Dom for testing. Allows testing without browser.Uses ReactDom but has additional functions to interact/events etc.

enzyme elem.dive(): to get inner component(if wrapped with connect etc)
elem.instance() // to get class to call elem.ComponentDidMount() etc
redux-mock-store package can be used for store, or createStore with initial and middlewares and wrap comp with provider and instead of shallow use mount(<Provider><InputEtc/>...)

```js
moxios.install(); // sets moxios as the axios adapter, routes axios calls to moxios insted of http, can pass axios instance to moxios install as argument(or blank for general axios)
moxios.wait(); // watches axios calls, sends response using callback passed to wait
// in .wait() callback, acces most recent request
const reques = moxios.requests.mostRecent();
request.respondWith(response); //specify response

// Action File Async Thunk Action
export const getSecret = () => (dispatch, getState) => {
  return axios
    .get("http://localhost:3030")
    .catch(e => console.log(e))
    .then(response => {
      dispatch({ type: actionTypes.SET_SECRET, payload: response.data });
    });
};

// Actions.test.js file
import moxios from "moxios";
import { storeFactory } from "../../testUtils/test-utils";
import { getSecret } from "./index.js";

describe("getSecretWord action creator", () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it("adds response word to state", () => {
    const secretWord = "party";
    const store = storeFactory();

    // moxios.wait -> starts listening requests from axios
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: secretWord // response.data will be used to acces secretWord
      });
    });

    return store.dispatch(getSecret()).then(response => {
      const newState = store.getState();
      expect(newState.secretWord).toBe(secretWord);
    });
  });
});
```

```js
// test utils file
export const findByTestAttr = (wrapper, name) =>
  wrapper.find(`[data-test="${name}"]`);

export const checkProps = (component, confirmingProps) => {
  const propError = checkPropTypes(
    component.propTypes,
    confirmingProps,
    "props",
    component.name
  );

  expect(propError).toBeUndefined();
};

export const storeFactory = initialState => {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares)
  );
};

// my solution to connected Component(redux)

const setup = (initialState = {}) => {
  const store = storeFactory(initialState);

  const element = mount(
    <Provider store={store}>
      <Input />
    </Provider>
  );

  return element.children().children(); // gets wrapper Input
};

describe("redux props", () => {
  it("has success piece of state as prop", () => {
    const success = true;
    const wrapper = setup({ success });
    const successProp = wrapper.instance().props.success; // .instance gets class(instance: to access class instance's props etc)
    expect(successProp).toBe(success);
  });

  it("guessedWord action creator is a function prop", () => {
    const wrapper = setup();
    const guessWordProp = wrapper.instance().props.guessWord;
    expect(typeof guessWordProp).toBe("function");
    //or expect(guessWordProp).toBeInstanceOf(Function)
  });
});
```

```js
import React from "react";
import Enzyme, { shallow } from "enzyme";
import EnzymeAdapter from "enzyme-adapter-react-16";
import App from "./App";

Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disablelifeCycleMethods: true //disables auto lifecycle method calls, they will run only when called
});

//create-react-app looks src/setupTests.js file for jest config
```

```js
describe("non connected App", () => {
  const getSecretMock = jest.fn(); //jest mock function
  const wrapper = shallow(
    <AppPlain getSecret={getSecretMock} success={false} guessedWords={[]} />
  );

  wrapper.instance().componentDidMount(); //because of the Enzyme config, it(lifecycle method) should be called explicitly

  // mockFunc.mock.calls is an array of arrays: [["args"], ["here"]]
  expect(getSecretMock.mock.calls.length).toBe(1);
});
```

```js
// test-util file
import checkPropTypes from "check-prop-types"; //npm i --save-dev

export const findByTestAttr = (wrapper, name) =>
wrapper.find(`[data-test="${name}"]`);

export const checkProps = (component, confirmingProps) => {
const propError = checkPropTypes(
component.propTypes,
confirmingProps,
"props",
component.name
);

expect(propError).toBeUndefined();
};

// .test.js file

describe("used to group tests", ()=>{
const wrapperEtc;
beforeEach(()=>{}) // runs before each test beforeEAch(cb)

it("it or test for describing test case", ()=>{}) })

it("does not throw warning with expected props", () => {
const expectedProps = { success: false };
checkProps(Congrats.propTypes, expectedProps);
});

test("counters starts at 0", () => {
const wrapper = setup();
const initialCounterState = wrapper.state("count");
expect(initialCounterState).toBe(0); // toBe ===, toEqual shallow comprassion
});

test("clicking on inc button increments counter display", () => {
const count = 7;
const wrapper = setup(null, { count });

// find increment button and click
const increment = findByTestAttr(wrapper, "counter-increment-button");
increment.simulate("click"); // ("click", { preventDefault(){}}) //second arg is optional event object

// find display and test value
const display = findByTestAttr(wrapper, "counter-display");
expect(display.text()).toContain(count + 1);
});

test("when counter is 0, decrement button click not sets count below 0 and state.error is set and it is removed once increment ", () => {
const count = 0;
const wrapper = setup(null, { count });
const incrementButton = findByTestAttr(wrapper, "counter-increment-button");
const decrementButton = findByTestAttr(wrapper, "counter-decrement-button");

decrementButton.simulate("click");
const errorDisplay = findByTestAttr(wrapper, "counter-error-display");
expect(errorDisplay.length).toBe(1);
expect(wrapper.state("error")).not.toBe("");

incrementButton.simulate("click");
expect(wrapper.state("count")).toBe(count + 1);
expect(wrapper.state("error")).toBe("");
});
```

### React Transition Group

```js
<TransitionGroup className="shopping-list">
  <CSSTransition key={id} timeout={1000} classNames="fade">
    {/*adds fade-enter, fade-enter-active, fade-exit/active to class*/}
    <ChildComponent />
  </CSSTransition>
</TransitionGroup>
```

```css
.fade-enter {
  opacity: 0.01;
}

.fade-enter-active {
  opacity: 1;
  transition: opacity 1s ease-in;
}

.fade-exit {
  opacity: 1;
}

.fade-exit-active {
  opacity: 0.01;
  transition: opacity 1s ease-in;
}
```

# ReactNative



