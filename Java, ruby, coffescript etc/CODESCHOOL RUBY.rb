RUBY


class CodeSchool do 
	def tweet(message, options = {})
		status = Status.new
		status.lat = options[:lat]
		status.body = message
		status.reply_id = options[:reply_id]
	end

	tweet("message", lat: 28.55, reply_id: 2565)

	def get_tweets(lists)
		unless list.authorized?(@user)
			raise AuthorizationException.new
		end
		list.tweets
	end

	begin 
		tweets = get_tweets(my_list)
	rescue AuthorizationException
		warn "you are not authorized"
	end

	def mention(status, *names) ->*means array
		tweet("#{names.join(" ")} #{status}")
	end

	.compact -> takes out nill values from array

	class UserList
		attr_accessor
		def initialize(name)
			self.name = name #calls name= on current object
		end
	end

	#protected -> hidden from outside, accesible for instances

	def User
		def initialize(name)
			@name = name
		end
	end

	class Follower < User
		def initialize(name, following)
			@following = following
			super(name) ----->calls parnt class's same named method
		def relationship
			"#{name} follows #{following}"
		end
	end

	if super called without arg it will use methods arg in parent meth

	def tweet(message, lat=nil, long=nil)-> default vals for lat long.first val is enough
	def asd(msg options={}) -> options hash for additional args.options hash can be omited
	def asd(x ,*y) -> * array argument.can be empty

	def get tweets(list)
		uness list.authorized(@user)
			raise AuthorizationException.new
		end
		list.tweets
	end

	when using tweets method

		begin
			tweets = get_tweets(my_list)
		rescure	AuthorizationException
			warn "asdasd"
		end

	class Name
		def initialize(first, last=nil)
			@first = first
			@last = last
		end
		def format
			[@last, @first].compact.join(", ")-> compact removes nil vals from arr
		end
	end

	user_names = []
	user_names << Name.new("asd", "asd")
	user_names << Name.new("Will")
	user_names.each { |n| puts n.format }

	class Tweet
		attr_accessor :status
		arrt_reader :created_at
		def initialize(status)
			@status = status
			@created_at = Time.new
		end
	end


	//MODULES

	image_utils.rb
		
	module ImageUtils
		def self.preview(image) //when used include self not needed
		end
	end

	run.tb

	image = user.image
	ImageUtils.preview(image)

	or

	class Image
		include ImageUtils -> will expose module's methods as class methods
	end

	when included 
	chained to ancestor 
	İmage.ancestors -> [Image, ImageUtils, Object, Kernel]
	Image.included_modules -> [ ImageUtils, Kernel] ->just included modules

	class Tweet
		extend -> included as class methods
	end

	Tweet.preview(img)

	module ImageUtils

		###or use method hooks and omit extend in Class
		#def self.included(base)
		##base.extend(ClassMethods)
		#end

		def preview
		end

		def transfer
		end

		module ClassMethods
			def fetch_from(x)
			end
		end
	end

	def Image
		include ImageUtils  -> takes ImageUtils methods
		extend ImageUtils::ClassMethods -> takes ImageUtils class methods
	end

	#or require "active_support/concern"

	module ImageUtils
		extend ActiveSupport::Concern

		#methods will be included
		#in Class which include ImageUtils
		#active recorc looks for ClassMethods
		#and injects those as class methods

	module ImageUtils
		extend ActiveSupport::Concern

		module ClassMethods
			def clearn up; end
		end
	end

	module ImageProcessing
		extend ActiveSupport:Concern
		include ImageUtils
		included do |
			clean_up
		end
	end

	class ImageProcessing
		include ImageProcessing
	end

	def cal_this_block
		block_result = yield "foo" -> takes argument
		puts block_result -> #oof
	end

	call_this_block { |arg| arg.reverse }

	##Procs 

	my_proc = Proc.new { puts "asd"}
	my_proc = lambda { puts "asda"}
	my_proc = -> { puts "ad"} #stabbing lambdas

	tweets.each(&printer) ->turns proc into block_result
	def each(&block) -> turns block into proc

	tweets.map [ |tweet| tweet.name]
	same as
	tweets.map(&:name) ->name.x not allowed

	is_block_given? -> for checking args with block

	my_proc.call

 end

class RubyMonk do

	1.methods // returns methods of 1
	arr.index("value") // returns index of value

	str.include? 'Yoda' , .start_with? "Ruby" , .end_with? 'Ruby'

	only false and nil equates to false

	for i in array
		puts i
	end

	sually_brown = Hash.new("brown") //default val for hash value

	chuck_norris = Hash[:punch, 99, :kick, 98, :stops_bullets_with_hands, true]
	-> returns chuck_norris = { :punch => 99 , :stops_bullets_with_hands =>true}	
	-> can be Hash[[:key ,value]]

	1.is_a?(Integer) 

	class Rectangle
	  def initialize(length, breadth)
	    @length = length
	    @breadth = breadth //@ means instance variables
	  end

	  def perimeter
	    2 * (@length + @breadth)
	  end
	end

	def add(a, b,c = 0) //default value for c
	  a + b + c
	end

	def add(*numbers) //* splat operator - multiple args
	  numbers.inject(0) { |sum, number| sum + number }
	end

	//js reduce like - 0 for default value 
	[4, 8, 15, 16, 23, 42].inject(0) do |accumulator, iterated|
	  accumulator += iterated
	  accumulator
	end

	def add(a_number, another_number, yet_another_number)
	  a_number + another_number + yet_another_number
	end

	numbers_to_add = [1, 2, 3] # Without a splat, this is just one parameter
		puts add(*numbers_to_add)  # Try removing the splat just to see what happens
	end
	-----
	def add(*numbers)
	  numbers.inject(0) { |sum, number| sum + number }
	end

	def add_with_message(message, *numbers)
	  "#{message} : #{add(*numbers)}"
	end

	puts add_with_message("The Sum is", 1, 2, 3)
	----
	def add(a_number, another_number, options = {})
	  sum = a_number + another_number
	  sum = sum.abs if options[:absolute]
	  sum = sum.round(options[:precision]) if options[:round]
	  sum
	end

	puts add(1.0134, -5.568)
	puts add(1.0134, -5.568, absolute: true)
	puts add(1.0134, -5.568, absolute: true, round: true, precision: 2)
	---
	def add(*numbers)
	  numbers.inject(0) { |sum, number| sum + number }  
	end

	def subtract(*numbers)
	  current_result = numbers.shift
	  numbers.inject(current_result) { |current_result, number| current_result - number }  
	end

	def calculate(*arguments)
	  # if the last argument is a Hash, extract it 
	  # otherwise create an empty Hash
	  options = arguments[-1].is_a?(Hash) ? arguments.pop : {}
	  options[:add] = true if options.empty?
	  return add(*arguments) if options[:add]
	  return subtract(*arguments) if options[:subtract]
	end
	---
	//lambda
	l = lambda { "Do or do not" }
	puts l.call
	//
	l = lambda do |string|
	  if string == "try"
	    return "There's no such thing" 
	  else
	    return "Do or do not."
	  end
	end
	puts l.call("try") 
	---
	def demonstrate_block(number)
	  yield(number)
	end

	puts demonstrate_block(1) { |number| number + 1 }
	---
	module WarmUp
	  def push_ups
	    "Phew, I need a break!"
	  end
	end

	class Gym
	  include WarmUp
	  
	  def preacher_curls
	    "I'm building my biceps."
	  end
	end

	class Dojo
	  include WarmUp
	  
	  def tai_kyo_kyu
	    "Look at my stance!"
	  end
	end

	puts Gym.new.push_ups
	puts Dojo.new.push_ups
	---
	module Perimeter
	  class Array
	    def initialize
	      @size = 400
	    end
	  end
	end

	our_array = Perimeter::Array.new //namespace
	ruby_array = Array.new
	---
	# module Gym
	#   class Push
	#     def up
	#       40
	#     end
	#   end
	# end
	require "gym"

	# module Dojo
	#   class Push
	#     def up
	#       30
	#     end
	#   end
	# end
	require "dojo" //imports .rb file named dojo

	dojo_push = Dojo::Push.new
	p dojo_push.up

	gym_push = Gym::Push.new
	p gym_push.up
	---
	IO
	# open the file "new-fd" and create a file descriptor:
	fd = IO.sysopen("new-fd", "w")

	# create a new I/O stream using the file descriptor for "new-fd":
	p IO.new(fd)
	---
	mode = "r+" //reading file
	file = File.open("friend-list.txt", mode)
	puts file.inspect
	puts file.read
	file.close
	---
	what_am_i = File.open("clean-slate.txt", "w") do |file|
	  file.puts "Call me Ishmael." //auto closes file if code block
	end

	p what_am_i

	File.open("clean-slate.txt", "r") {|file| puts file.read }
	---
	//reading file
	somefile = File.open("sample.txt", "w") //a for appending
	somefile.puts "Hello file!"
	somefile.close
  --- //writing a page data to a local file
  remote_base_url = "http://en.wikipedia.org/wiki"
	remote_page_name = "Ada_Lovelace"
	remote_full_url = remote_base_url + "/" + remote_page_name

	remote_data = open(remote_full_url).read
	my_local_file = File.open("my-downloaded-page.html", "w") 

	my_local_file.write(remote_data)
	my_local_file.close
	---
	require 'rubygems'
	require 'rest-client'

	wiki_url = "http://en.wikipedia.org/"
	wiki_local_filename = "wiki-page.html"

	File.open(wiki_local_filename, "w") do |file|
	   file.write(RestClient.get(wiki_url))
	end
	---
	file = File.open("sample.txt", 'r')
	while !file.eof? //.eof? returns true if no line left
	   line = file.readline
	   puts line
	end
	---
	require 'open-uri'
	url = "http://ruby.bastardsbook.com/files/fundamentals/hamlet.txt"
	puts open(url).readline
	#=> THE TRAGEDY OF HAMLET, PRINCE OF DENMARK 
	---
	require 'open-uri'         
	url = "http://ruby.bastardsbook.com/files/fundamentals/hamlet.txt"
	local_fname = "hamlet.txt"
	File.open(local_fname, "w"){|file| file.write(open(url).read)}

	File.open(local_fname, "r") do |file|
	   file.readlines.each_with_index do |line, idx|
	      puts line if idx % 42 == 41
	   end   
	end
	---
	is_hamlet_speaking = false
	File.open("hamlet.txt", "r") do |file|
	   file.readlines.each do |line|

	      if is_hamlet_speaking == true && ( line.match(/^  [A-Z]/) || line.strip.empty? )
	        is_hamlet_speaking = false
	      end

	      is_hamlet_speaking = true if line.match("Ham\.")

	      puts line if is_hamlet_speaking == true
	   end   
	end
	---
	dirname = "data-files"
	Dir.mkdir(dirname) unless File.exists?dirname
	File.open("#{dirname}/new-file.txt", 'w'){|f| f.write('Hello world!')}   
	---
	# count all files in my Downloads directory and in sub-directories
	puts Dir.glob('Downloads/**/*').length   #=> 308858

	# list just PDF files, either with .pdf or .PDF extensions:
	puts Dir.glob('Downloads/*.{pdf,PDF}').join(",\n")
	---

class BegginingRuby2nd do

	//CLASS//

	x = 2 //basic variables/no var or int x 

	class Person
		attr_accessor :name, :age, :gender //constructor-local vars for every instance

		def talk
			puts "person talk method-function"
		end
	end
	person_instance = Person.new 

	class Cat < Pet //cat class inherits from Pets class

	--STRING METHODS--

	"Test" + "Test" -> "TestTest" // .downcase .capitalize .chop(removes last one-> Tes) .next(last one increased -> Tesu)
	.reverse .sum .swapcase .upcase .

	--FLOW CONTROL--
	condition ? whentrue : whenfalse -> ternay 

	fruit = "orange"  -> case
	color = case fruit
					when "orange"
						"orange"
					when "yellow"
						"yellow"
					else
						"unknow"
					end

	if elsif else // unless -> opposite of if  // until

	1.upto(5) // 5.downto(5) // 0.step(50,5) -> upto 50 with spep of five 5-10-...-50 //.to_f -> converts number to floating number // to_i -> to int
	Pi = 3.14 -> Constant // .class for learning class of object

	//STRING METHODS//

	x = %{ this is a          //{} optional can be [] or else // x = <<END_MARK multiline string END_MARK
				test of multi line} 

	?x for learning char value(asci value) of string

	puts "#{interpolation_var} + hmm"

	puts "foobar".sub("bar", "foo") --> changes first item with last one // gsub("i", "") -> for multiple substution
	puts "this is test".gsub(/^../, "Hello") --> REGEX ^ start of line $ end of line - \A absolute start of string \Z end of string
	"xyz".scan(/./) { |letter| puts letter } ->.scan methods iterates over a string and passes to code block if regex matched
	x = "Match method lets u acces data".match(/(\w+) (\w+)/) -> x[0] -> whole match x[1] -> match of grup1 x[2] -> match group2 

	--ARRAY METHODS--
	x << "word" -> pushes "word" to x array equivalent to x.push("word")
	x.pop -> removes last item from array // x.length // x.join //x.join(",") // string.split(/\./)

	.inspect -> for textual presentation of object -> puts x.inspect or shortcut p x

	array.each // array.map or equi -> array.collect
	x + y -> concats array or x-y for substacting array
	.empty? -> checking for if array has values // x.include("a") -> checks arrs values includes "a"-returns true/false
	x.first // x.first(2) -> arrs first 2 value // x.last // x.last(2)  // arr.reverse

	--HASHES--
	dictionart = { "cat" => "value"} -> keys can be any obj type
	dictionary.size // dict.keys -> returns array of keys // x.delete("key") -> removes from hash // .delete_if {|key, value| value < 25}

	--PROCS YIELD LAMBDA--

	def each_vowel(&code_block) //proc
		%w{a e i o u}.each{ |vowel| code_block.call(vowel) }
	end

	each_vowel { |vowel| puts vowel }

	def each_vowel(&code_block)  //yield
		%w{a e i o u}.each{ |vowel| yield vowel }
	end

	def demonstrate_block(number) //yield2
	  yield(number)  // can be used without args but if there is not codeblock will throw exception-no code block given
	end

	puts demonstrate_block(1) { |number| number + 1 } //prints 2

	each_vowel { |vowel| puts vowel }

	print_param_to_screen = lambda { |x| puts x }  //lambda
	print_oarameter_to_screen.call(100)


	Time.now -> Thu Apr 16 00:00:00 +0100 2009 -> GMT JAN 1 (starts from)
