### VSCode

- ctrl + p: searching files-supports fuzzy searching(u) //hitting p moves cursor down(prev files list)
- ctrl + shift + p or f1 on windows: command palette
- shift + ctrl + f : searching things in files/or left panel search
- ctrl + back space: deletes newly created file
- ctrl + , : settings
- ctrl + b : hide/show sidebar

- advanced-new-file extension downloaded -> ctrl + alt + n : create new file
- file utils extension for file renamin
- php intelephense extension for php support(ctrl o: goes def/function)-ctrl + t : to go a defination/func

- snippet-creator for creating snippets from existing block of code : select code, ctrl + shift + p (command palette) create snippet -> scope(php etc), trigger, description
- snippet tab completion is false by def -> go settings and change tab comp to true

- view > scm : to view git differences / can be seen in file too
- settings -> editor.minimap.enabled: false to remove right code bar


- changing terminal to git terminal instead of windows cmd: toggle terminal setting

```js
"terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe",
"terminal.integrated.shellArgs.windows": [
  "-l",
  "-i"
]    
```

### Sublime Text

https://packagecontrol.io/installation // for installing other things we need package control

- ctrl + shift + p : package controll // for installing, type install, select, write package name
- PackageResourceViewer // allows to view package files // ctrl+shift+p -> extract package -> >preferences>browse packages

- ctrl + p // for accesing files in your system
- ctrl + p then enter: switches to last file
- ctrl + r // going to a defination // @asd -> goes asd function
- ctrl + alt + p // going to method/func defination, checks all files

- AdvancedNewFile package > ctrl + alt + n : new file
- ANF has compleation> h+tab -> /html
- ANF -> :newFile -> : means current directory
- ANF -> package control -> there is delete, rename options

- alt + shift + 2 => two panel, +3 for three, +1 for returning normal // there is also a package for advance(origami)

- ctrl + d // for selecting the word, ctrl + d will select same ones
- alt + f3 // select all words


- there is a vintage mode // prolly in ignored packages // when pressed esc, its goes into a vim like mode

### Snippets

- Tools -> Developer -> New Snippet
- ${1:}: PropTypes.string.${2:optionalTextForComment}, // text after colon in field marker is optional
- `<tabTrigger>hello</tabTrigger>` // optional, trigger keyword
- <scope> source: python </scope> // where it can be invoked
- snippets : must be saved as abc.sublime-snippet, ${1:default} // $: stop points ${1:first}, $0 is exit point
- <description> demo description </description> // this text is shown when typing

- Php Getters and Setters package // ctrl shift p -> getters and setters -> 
- // auto creates getter/setter by reading __construct

- Php Companion // for use fallclasspath and adding use statement to top 
- // user keys -> f9: full class path, f5: auto import file at top, f4: creates construct + props for it
- // u can change prop visibility -> visibility: protected, use_sort_length: true

- phpunit package for testing
- { "keys": [",", "p"], "command": "phpunit" },
- { "keys": [",", "f"], "command": "phpunit_current_file" }


- laravel 5 artisan // for running artisan commands from sublime


- composer global require fabpot/php-cs-fixer
- php-cs-fixer fix fileName --level="psr2"
- >tools>buildsystems>new build system
- { "shell_cmd":  "php-cs-fixer fix $file --level=psr2"} // save
- tools>bs>select new bs ---> f7
- "show_panel_on_build": false // settings changed for no panel output

- sublimelinter + sublimelinter-php packages for lint
- "show_errors_on_save": true // added to user setting

##### sublime command line
// http://docs.sublimetext.info/en/latest/command_line/command_line.html
- ln -s "/Applications/Sublime Text 2.app/Contents/SharedSupport/bin/subl" ~/bin/subl
// subl . // opening current dir
// subl filename // opening the file in sublime


##### key bindings
```
[
	{"keys": ["alt+shift+f"], "command": "reindent", "args": {"single_line": false}},
	{ "keys": ["ctrl+7"], "command": "toggle_comment", "args": { "block": false } },
	{ "keys": ["ctrl+shift+7"], "command": "toggle_comment", "args": { "block": true } },

	{ "keys": ["f9"], "command": "expand_fqcn" },
	{ "keys": ["f5"], "command": "find_use" },
	{ "keys": ["ctrl+b"], "command": "toggle_side_bar" },
	{ "keys": ["f4"], "command": "insert_php_constructor_property" }
]
```