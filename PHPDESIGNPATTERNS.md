### PHP DESIGN PATTERNS

##### Adapter Pattern
- a new class that essentially performs the same task as an old class, but it has different names for the methods.
```
// original class

class PayWithPayZilla {
 
  function addItem($itemName)
  {
    var_dump("1 item added: " . $itemName );
  }
  	
  function addPrice($itemPrice)
  {
    var_dump("1 item added to total with the price of: " . $itemPrice );
  }
}

class PayKal { // new class-doing same things with diff method names
  function addOneItem($name)
  {
    var_dump("1 item added: " . $name);
  }
 	
  function addPriceToTotal($price)
  {
    var_dump("1 item added to total with the price of: " . $price);
  }
 	
  // Unique method
   addItemAndPrice($name,$price)
  {
    $this -> addOneItem($name);
    $this -> addPriceToTotal($price);
  }
}

// extracting original's methods to an interface 
interface PayZilla {
  function addItem($itemName);
 	
  function addPrice($itemPrice);
}

// using that interface on original class
class PayWithPayZilla implements PayZilla { ... }

// adapter class for wrapping new class methods with old name

class PayKal2PayZillaAdapter implements PayZilla {
           
  // The adapter holds a reference to the new class.
  private $payObj;
   
  // In order to hold a reference, we need to pass the new 
  //   class's object throught the constructor.
  function __construct($payObj)
  {
    $this -> payObj = $payObj; 
  }
  
  // The name of the methods is that of the old class.
  // The code within the methods uses the code of the new class.
  function addItem($itemName)
  {
    $this -> payObj -> addOneItem($itemName);
  }
}

$payKal = new PayKal();
$pay = new PayKal2PayZillaAdapter($payKal);
$customer = new Customer($pay);
$customer -> buy("lollipop", 2);

```

##### FACTORY PATTERN
- when we need to free a class from making the objects that it manages
```
class CarFactory {
 
  protected $car;
  
  // Determine which model to manufacture, and instantiate 
  //  the concrete classes that make each model.
  public function make($model=null)
  {
    if(strtolower($model) == 'r')
      return $this->car = new CarModelR();
  
    return $this->car = new CarModelS();
  }
}

class CarOrder {
  protected $carOrders = array();
  protected $car;
  
  // First, create the carFactory object in the constructor.
  public function __construct()
  {
    $this->car = new CarFactory();
  }
  
  public function order($model=null)
  {
    // Use the make() method from the carFactory.
    $car = $this->car->make($model);
    $this->carOrders[]=$car->getModel();
  }
  
  public function getCarOrders()
  {
    return $this->carOrders;
  }
}


interface Car {
  function getModel();
  
  function getWheel();
  
  function hasSunRoof();
}


class CarModelS implements Car {
  protected $model = 's';
  protected $wheel = 'sports';
  protected $sunRoof = true;
  
  public function getModel()
  {
    return $this->model;
  }
  
  public function getWheel()
  {
    return $this->wheel;
  }
  
  public function hasSunRoof()
  {
    return $this->sunRoof;
  }
}

class CarModelR implements Car { ... }


$carOrder = new CarOrder;
var_dump($carOrder->getCarOrders());
 
$carOrder->order('r');
var_dump($carOrder->getCarOrders());
 
$carOrder->order('s');
var_dump($carOrder->getCarOrders());

```

##### TEMPLATE PATTERN
- All we need to do is to convene the duplicated code from the classes into an abstract parent class
```
// The abstract parent class.
abstract class Book {
  protected $title;
  protected $content;
     
  function setTitle( $str )
  {
    $this->title = $str;
  }
    
  function setContent( $str )
  {
    $this->content = $str;
  }
}
 
class Paperback extends Book {
      
  function printBook()
  {
    var_dump("The book '{$this->title}' was printed.");
  }
}
 
class Ebook extends Book {
      
  function generatePdf()
  {
    var_dump("A PDF was generated for the eBook '{$this->title}'.");
  }
}
```

##### FACADE PATTERN
When we want to use consists of too many classes and methods, and all we want 
is a simple interface, preferably one method, that can do all the job for us

```
// The Facade class
class shareFacade {
  // Holds a reference to all of the classes.
  protected $twitter;    
  protected $google;   
  protected $reddit;    
    
  // The objects are injected to the constructor.   
  function __construct($twitterObj,$gooleObj,$redditObj)
  {
    $this->twitter = $twitterObj;
    $this->google  = $gooleObj;
    $this->reddit  = $redditObj;
  }  
        
  // One function makes all the job of calling all the share methods
  //  that belong to all the social networks.
  function share($url,$title,$status)
  {
    $this->twitter->tweet($status, $url);
    $this->google->share($url);
    $this->reddit->reddit($url, $title);
  }
}

// Create the objects from the classes.
$twitterObj = new CodetTwit();
$gooleObj   = new Googlize();
$redditObj  = new Reddiator();

// Pass the objects to the class facade object.
$shareObj = new shareFacade($twitterObj,$gooleObj,$redditObj);

// Call only 1 method to share your post with all the social networks.
// instead of $twitterObj->tweet(...); googleObj... redditObj....
$shareObj->share('http://myBlog.com/post-awsome','ma post','read ma post.');

```

##### SINGLETON PATTERN
We use the singleton pattern in order to restrict the number of instances 
that can be created from a resource consuming class to only one.

Resource consuming classes are classes that might 
slow down our website or cost money. For example:

- Some external service providers (APIs) charge money per each use.
- Some classes that detect mobile devices might slow down our website.
- Establishing a connection with a database is time consuming and slows down our app.
- 
So, in all of these cases, it is a good idea to restrict the number of objects 
that we create from the expensive class to only one.

```
// Singleton to connect db.
class ConnectDb {
  // Hold the class instance.
  private static $instance = null;
  private $conn;
  
  private $host = 'localhost';
  private $user = 'db user-name';
  private $pass = 'db password';
  private $name = 'db name';
   
  // The db connection is established in the private constructor.
  // private: to prevent initiation with outer code.
  private function __construct()
  {
    $this->conn = new PDO("mysql:host={$this->host};
    dbname={$this->name}", $this->user,$this->pass,
    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
  }
  
  public static function getInstance()
  {
    if(!self::$instance)
    {
      self::$instance = new ConnectDb();
    }
   
    return self::$instance;
  }
  
  public function getConnection()
  {
    return $this->conn;
  }
}

$instance = ConnectDb::getInstance();
$conn = $instance->getConnection();
```

##### DECORATOR PATTERN
to add new optional classes to existing basic classes without having to change 
the codes of the latter, thereby helping us to avoid imposing 
additional responsibilities on classes that were already tested.
```
// at first
interface Car {
  function cost();
  function description();
}

class Suv implements Car {
    function cost()
    {
      return 30000;
    }

    function description ()
    {
      return "Suv";
    }
}

// we wanna add new features without changing original

abstract class CarFeature implements Car {
  protected $car;

  function __construct(Car $car)
  {
    $this->car = $car;
  }

  abstract function cost();
  
  abstract function description();
}

// subclasses that inherits decorator

class SunRoof extends CarFeature {
    function cost ()
    {
        return $this->car->cost() + 1500;
    }

    function description()
    {
        return $this->car->description() . ",  sunroof";
    }
}

class HighEndWheels extends CarFeature { ... } etc


// Create an object from one of the basic classes.
$basicCar = new Suv();

// Pass the object from the basic class as a parameter to the feature class.
$carWithSunRoof = new SunRoof($basicCar);

echo $carWithSunRoof -> cost();
```

##### STRATEGY PATTERN
We consider use of the strategy pattern when we need to choose between 
similar classes that are different only in their implementation
- classes with same interface, a chooser func/class for making those, a class that uses classess

```
interface carCouponGenerator {
  function addSeasonDiscount();
  function addStockDiscount();
}

class bmwCouponGenerator implements carCouponGenerator {
  private $discount = 0;
  private $isHighSeason = false;
  private $bigStock = true;
    
  public function addSeasonDiscount()
  {
    if(!$this->isHighSeason) return $this->discount += 5;
  
    return $this->discount +=0;
  }
    
  public function addStockDiscount()
  {
    if($this->bigStock) return $this->discount += 7;
      
    return $this->discount +=0;
  }
}

class mercedesCouponGenerator implements carCouponGenerator { ... }

function couponObjectGenerator($car)
{
  if($car == "bmw")
  {
    $carObj = new bmwCouponGenerator;
  }
  else if($car == "mercedes")
  {
    $carObj = new mercedesCouponGenerator;
  }
      
  return $carObj;
}

// The client class generates coupon from the object of choice.
class couponGenerator {
  private $carCoupon;
  
  // Get only objects that belong to the interface.  
  public function __construct(carCouponGenerator $carCoupon)
  {
    $this->carCoupon = $carCoupon;
  }
   
  // Use the object's methods to generate the coupon. 
  public function getCoupon()
  {
    $discount = $this->carCoupon->addSeasonDiscount();
    $discount = $this->carCoupon->addStockDiscount();
    
    return $coupon = "Get {$discount}% off the price of your new car.";
  }
}

$car = "bmw";
$carObj = couponObjectGenerator($car);
$couponGenerator = new couponGenerator($carObj);
echo $couponGenerator->getCoupon();

```


##### CHAIN OF RESPONSIBILITY
There is a abstract class with a successor prop and neccessary abstract methods
Abstract class have a succesor and next methods
child class sends data if condition true via next nethod

```
abstract class HomeChecker{
  protected $successor;
  
  public abstract function check(HomeStatus $home);
  
  public succeedWith(HomeChecker $obj){
    $this->successor = $obj;
  }
  
  public function next(HomeStatus $home){
    if($this->successor){
      this->successor->check($home);  
    }
  }
}


class Lock extends HomeChecker{
  public function check(HomeStatus $home){
    if(!$home->locked){
      throw new Exception("doors not locked");
    }
    next($home);
  }
}

class Lights extends HomeChecker{...}
class Alarm extends HomeChecker{...}

class HomeStatus {
  public $locked = true;
  public $lightsOff = true;
  public $alarmOn = true;
}

$lock = new Lock();
$light = new Light;
$alarm = new Alarm;

$lock->succesWith($light);
$light->succesWith($alarm);

// will call next til there is no successor
$lock->check(new HomeStatus);
```
