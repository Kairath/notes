# Graph Query Language (Spesificcation not Implementation)

query: fetch data
mutation: crud operations
subscription: on data change server(via websockets) updates client
Graph = types with fields and associations between them
Prisma: GrahpQL ORM (supports mysql, mongodb etc) / prisma init projectName

typeDefs can be saved in schema.graphql file
const server = new GraphQLServer({typeDefs: "./path/schema.grapql", resolvers})

String!: always(! means always) returns String
[]: return value is an array, []!: always returns array
fieldName(argName: String) // when queried-> fieldName(argName: "a")

input CreateUserInput { } //instead of data: {a: String, b...}, input can be used data: CreateUserInput

enum MutationType {
CREATED
UPDATED
DELETED
}

```
query {
	field
	field2withsubfields{
		subfield
	}
	greeting(argOfName: "stringOrSomethingElse")
}

//returns data { field:}
```

### GRAPHQL YOGA - server side implementation

```js
import { GraphQLServer, PubSub } from "graphql-yoga";

const pubSub = new PubSub(); // to publish/subscribe

// Type Definations(Schema): Describes types/operations
// Resolvers: functions that runs when operations run

// Scalar Types - String, Boolean, Int, Float, ID

`greeting(name: String): String!` has one optional arg(no !), and arg is string

const typeDefs = `
	type Query {
		hello: String!
		isStock: Boleean
		me: User
		post: Post
		greeting(name: String, position: String): String!
		grades: [Int!]! (always returns an array and always contains an int)
		add(numbers: [Float!]!): Float!
		users(query: String): [User!]!
	}

	type Mutation {
		createUser(name: String!, email: String!, age: Int): User!
  }

  type Subscription {
    post: PostSubscriptionPayload!
  }

  type PostSubscriptionPayload { # common convention to name it SomethingSubscriptionPayload
    mutation: String!
    data: Post!
  }

	type User {
		id: ID!
		name: String!
		email: String!
		age: Int!
	}

	type Post {
		id...
		author: User! // needs a resolver(not in query) to get User data, parent has id of user in posts array/db
	}



`;
// resolvers
const resolvers = {
  Query: {
    hello() {
      return "this is my first query";
    },
    isStock() {
      return true;
		},
		me: ()=>({
			id: ()=> "32342",
			name(){ return "asd" },
			...
		}),
		greeting(parent, args, ctx, info){ // parent(relational db: post has many, args passed down, ctx: context)
			return `Hello ${args.name || "stranger" }. You are a ${args.position || "something"}`
		},
		grades(parent, args, ctx, info){
			return [33, 44, 28];
		},
		users(parent, args, ctx, info){
			if(args.query) {
				return users.filter(user=>user.username.includes(query))
			}
			return users;
		}
	},
	Mutation: {
    createUser(parent, args, ctx, info) {
      const emailTaken = users.some(user => user.email === args.email);

      if (emailTaken) throw new Error("Email is taken");

      let user = {
        name: args.name,
        email: args.email,
        age: args.age,
        id: uuidv4()
      };

      users.push(user);

      return user;
    },
	Post: {
		// grapql will call for this function with Post as parent
		author(parent, args, ctx, info){
      return users.find(user => user.id == parent.author);
		}
  }

  Subscribtion: {
    count: {
      subscribe(parent, args, { pubsub }, info) {
        let count = 0;

        setInterval(() => {
          count++;
          pubsub.publish("count", {
            // object key matches name of subfield which is count here
            // if CountSubscriptionPayload etc used
            count: {
              mutation: "UPDATEetc",
              data: commentEtc
            }
          });
        }, 1000);

        return pubsub.asyncIterator("count"); // if i
      }
    },
    something: {
      subcribe(p, a, c, i) {
        return pubsub.asyncIterator("channel name: comment postId etc");
      }
    }
  }
};

//context will be passed down to all resolvers(ctx arg)
const server = new GraphQLServer({
	typedefs, resolvers, context: {dbEtc}
});

server.start(() => {
  console.log("server is up");
});
```

`"start": "nodemon src/index.js --exec babel-node"` babel(babel-cli, babel-preset-env) with nodemon, babelrc: {"presets": ["env"]}

## Apollo Graphql

```js
// .env file
ENGINE_API_KEY=service:my-service-439:E4VSTiXeFWaSSBgFWXOiSA

// apollo config file
module.exports = {
  client: {
    name: 'Space Explorer [web]',
    service: 'space-explorer',
  },
};

// client
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';//import {WebSocket} from "apollo-link-ws" for subscriptions
import gql from "graphql-tag";

const cache = new InMemoryCache();
const link = new HttpLink({
  uri: 'http://localhost:4000/'
})
const client = new ApolloClient({
  cache,
  link
})

client
  .query({
    query: gql`
      query GetLaunch {
        launch(id: 56) {
          id
          mission {
            name
          }
        }
      }
    `
  })
  .then(result => console.log(result));

// react
import { ApolloProvider } from 'react-apollo';

ReactDOM.render(
  <ApolloProvider client={client}>
    <Pages />
	</ApolloProvider>, document.getElementById('root'));

// react component
import React, { Fragment } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const GET_LAUNCHES = gql`
	query launchList($after: String) { ...`

// consumer = query, mutation
<Query query={GET_LAUNCHES}>
      {({ data, loading, error }) => {

<ApolloConsumer>
      {client => (
        <Mutation
          mutation={LOGIN_USER}
```

```js
const { ApolloServer } = require("apollo-server");
const typeDefs = require("./schema");

const server = new ApolloServer({ typeDefs });
```

```js
const { gql } = require("apollo-server");

const typeDefs = gql`
  type Query {
    launches: [Launch]!
    launch(id: ID!): Launch
    # Querry for current user
    me: User
  }

  enum SIZE {
    SMALL
    LARGE
  }
`;

module.exports = typeDefs;
```

## Hashura.io Course

```graphql
query($varName: Int!) { // now query needs extra variable object {"varName": 5}
  users(limit: 10) { // here we can say limit: $limit
    name
    todos(order_boy: {created_at: desc, limit: 5}){
      title
    }
  }
}

mutation($todo: todos_insert_input!) { //with query send { "todo": {"title": "a dynamic todo"}}
  insert_todos(objects: [$todo]){
    returning{
      id
    }
  }
}

mutation ($todo: String!, $isPublic: Boolean!) { // variable obj with query->{"todo": "hm", "isPublic": ture}
  insert_todos(objects: {title: $todo, is_public: $isPublic}) {
    affected_rows
    returning {
      title
    }
  }
}

subscription {
  online_users {
    last_seen
    user{
      name
    }
  }
}
```

`npm i --save apollo-client react-apollo apollo-cache-inmemory apollo-link-http graphql graphql-tag`
`npm install apollo-link-ws subscriptions-transport-ws --save` // instead of http-link for subcriptions

```js
// App.js

import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import { ApolloProvider } from "react-apollo";

const createApolloClient = authToken => {
  return new ApolloClient({
    link: new HttpLink({
      uri: "https://learn.hasura.io/graphql",
      headers: {
        Authorization: `Bearer ${authToken}`
      }
    }),
    cache: new InMemoryCache()
  });
};


const App = ({ auth }) => {
  const client = createApolloClient(auth.idToken);

  return <ApolloProvider client={client}>// other components</ApolloProvider>;
};

// in related component file
import { Query } from "react-apollo";
import gql from "graphql-tag";

const GET_MY_TODOS = gql`
  query getMyTodos {
    todos(
      where: { is_public: { _eq: false } }
      order_by: { created_at: desc }
    ) {
      id
      title
      created_at
      is_completed
    }
  }
`;

const TodoPrivateListQuery = () => {
  return (
    //recomended way is
    //<Query query={QUERY_STR}>{({ loading, error, data, client }) => { //components } </Query>
    <Query query={GET_MY_TODOS}>
      {" "}
      {/* or by using the query method directly and then process the response.*/}
      {/* second way ->
        <ApolloConsumer>
        {client => (...
        <Button
              onClick={async () => {
                const { data } = await client.query({
                  query: GET_DOG_PHOTO,
                  variables: { breed: "bulldog" }
                });
                this.onDogFetched(data.dog);
              }}
        />

      */}
      {({ loading, error, data, client }) => {
        if (loading) {
          return <div>Loading...</div>;
        }
        if (error) {
          console.error(error);
          return <div>Error!</div>;
        }
        return <TodoPrivateList client={client} todos={data.todos} />; //fetched data passed as prop
      }}
    </Query>
  );
};

//insert-todo.js
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const ADD_TODO = gql`
  mutation($todo: String!, $isPublic: Boolean!) {
    insert_todos(objects: { title: $todo, is_public: $isPublic }) {
      affected_rows
      returning {
        id
        title
        created_at
        is_completed
      }
    }
  }
`;

const TodoInput = ({ isPublic = false }) => {
  const [todoInput, setTodoInput] = useState('');

  const updateCatche = (cache, {data}) => {
    // If this is for the public feed, do nothing
    if (isPublic) {
      return null;
    }
    // Fetch the todos from the cache, readsing from current store/catche
    const existingTodos = cache.readQuery({
      query: GET_MY_TODOS
    });
    // Add the new todo to the cache
    const newTodo = data.insert_todos.returning[0]; // data.fieldName.subField[0]
    cache.writeQuery({ //updates cache but not db // db already updated with mutation
      query: GET_MY_TODOS,
      data: {todos: [newTodo, ...existingTodos.todos]}
    });
  };
  }

  const resetInput = () => {
    setTodoInput('');
    input.focus();
  };

  return (
     <Mutation mutation={ADD_TODO} update={updateCache} onCompleted={resetInput}>
      {/*
      Mutation has 4 optional prop-> variables, optimisticResponse, refetchQueries and update
      first rendered prop is mutation function which will be called when adding a new todo
      addTodo({variables: {title: todoInputWhichIsInputsValue, isPublic}})
      When db is updated/mutation ran update fired with arguments (cache, {data})
      After mutation is completed/executed resetInput function will be called

    */}
      {(addTodo, { loading, data }) => {
        return (
          <form
            className="formInput"
            onSubmit={e => {
              e.preventDefault();
              addTodo({ variables: { title: "title ", isPublic } });
            }}
          >
            <input className="input" placeholder="What needs to be done?" ref={inputRef}/>
            <i className="inputMarker fa fa-angle-right" />
          </form>
        );
      }}
    </Mutation>
  );
};


// To acces client we need to wrap Component with withApollo
import {withApollo} from 'react-apollo';
const TodoItem = ({index, todo, client}) => {
export default withApollo(TodoItem);

// updating/mutation with client
 const toggleTodo = () => {
    client.mutate({
      mutation: TOGGLE_TODO,
      variables: {id: todo.id, isCompleted: !todo.is_completed},
      optimisticResponse: {},
      update: (cache) => {
        const existingTodos = cache.readQuery({ query: GET_MY_TODOS });
        const newTodos = existingTodos.todos.map(t => {
          if (t.id === todo.id) {
            return({...t, is_completed: !t.is_completed});
          } else {
            return t;
          }
        });
        cache.writeQuery({
          query: GET_MY_TODOS,
          data: {todos: newTodos}
        });
      }
    });
  };

// Subscription

// Subcription Support setting / instead of http-link
const createApolloClient = (authToken) => {
  return new ApolloClient({
   link: new WebSocketLink({
     uri: 'wss://learn.hasura.io/graphql',
     options: {
       reconnect: true,
       connectionParams: {
          headers: {
            Authorization: `Bearer ${authToken}`
          }
       }
     }
    }),
    cache: new InMemoryCache(),
  });
};

// wrap components return with Subcription

 return (
   <div className="onlineUsersWrapper">
    <Subscription subscription={gql`
      subscription getOnlineUsers {
        online_users(order_by: {user: {name: asc }}) {
          id
          user {
            name
          }
        }
      }`}>
      {({ loading, error, data }) => {
        if (loading) {
          return (<span>Loading...</span>);
        }
        if (error) {
          console.error(error);
          return (<span>Error!</span>);
        }
        if (data) {
          const users = data.online_users;
          const onlineUsersList = [];
          users.forEach((u, index) => {
            onlineUsersList.push(
              <OnlineUser
                key={index}
                index={index}
                user={u.user}
              />);
          });
          return (
            <Fragment>
              <div className="sliderHeader">
                Online users - {users.length}
              </div>
              {onlineUsersList}
            </Fragment>
          );
        }
      }}
    </Subscription>
   </div>
 );

```

### Apollo Tutorial

```js
```
